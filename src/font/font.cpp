#include "font.h"

namespace breakout
{
	namespace fonts
	{
		Font option;
		Font title;
		Color optionSelected = YELLOW;
		Color optionNoSelected = WHITE;
		float titleSpacing = 10.0f;
		float optionSpacing = 5.0f;

		void drawCenterText(Font font, const char* text, int posX, int posY, int fontSize, float spacing, Color color)
		{
			Vector2 pos;
			pos.x = static_cast<float>((posX - (MeasureTextEx(font, text, static_cast<float>(fontSize), spacing).x / 2)));
			pos.y = static_cast<float>((posY - (fontSize / 2)));
			DrawTextEx(font, text, pos, static_cast<float>(fontSize), spacing, color);
		}

		void init()
		{
			option = LoadFont("res/assets/fonts/option.otf");
			title = LoadFont("res/assets/fonts/title.ttf");
		}

		void deInit()
		{
			UnloadFont(option);
			UnloadFont(title);
		}
	}
}