#pragma once
#ifndef FONTS_H
#define FONTS_H

#include "raylib.h"

namespace breakout
{
	namespace fonts
	{
		extern Font option;
		extern Font title;
		extern Color optionSelected;
		extern Color optionNoSelected;
		extern float titleSpacing;
		extern float optionSpacing;

		void drawCenterText(Font font, const char* text, int posX, int posY, int fontSize, float spacing, Color color);
		void init();
		void deInit();
	}
}

#endif // !FONT_H