#pragma once
#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace breakout
{
	namespace audio
	{
		extern Music gameplayMusic;
		extern Music menuMusic;
		extern Sound popSound;
		extern Sound optionSound;
		extern Sound scoreSound;
		extern Sound explosionSound;

		extern float musicVolume;
		extern float sfxVolume;

		void init();
		void deInit();

		void setMusicVolume(float volume);
		void setSfxVolume(float volume);
	}
}

#endif // !AUDIO_H