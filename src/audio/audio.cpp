#include "audio.h"

namespace breakout
{
	namespace audio
	{
		Music gameplayMusic = LoadMusicStream("res/assets/music/gameplay-soundtrack.mp3");
		Music menuMusic = LoadMusicStream("res/assets/music/menu-soundtrack.mp3");
		Sound popSound = LoadSound("res/assets/sounds/pop.ogg");
		Sound optionSound = LoadSound("res/assets/sounds/menu-selection.ogg");
		Sound scoreSound = LoadSound("res/assets/sounds/score.ogg");
		Sound explosionSound = LoadSound("res/assets/sounds/explosion.ogg");

		float musicVolume = 0.6f;
		float sfxVolume = 0.8f;

		void init()
		{
			InitAudioDevice();
			setMusicVolume(musicVolume);
			setSfxVolume(sfxVolume);
		}

		void deInit()
		{
			UnloadMusicStream(gameplayMusic);
			UnloadMusicStream(menuMusic);
			UnloadSound(popSound);
			UnloadSound(optionSound);
			UnloadSound(scoreSound);
			UnloadSound(explosionSound);
			CloseAudioDevice();
		}

		void setMusicVolume(float volume)
		{
			musicVolume = volume;
			SetMusicVolume(gameplayMusic, volume);
			SetMusicVolume(menuMusic, volume);
		}

		void setSfxVolume(float volume)
		{
			sfxVolume = volume;
			SetSoundVolume(popSound, volume);
			SetSoundVolume(optionSound, volume);
			SetSoundVolume(scoreSound, volume);
			SetSoundVolume(explosionSound, volume);
		}
	}
}