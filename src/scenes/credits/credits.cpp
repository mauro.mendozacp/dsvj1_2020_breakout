#include "credits.h"
#include "game/game.h"

namespace breakout
{
	namespace credits
	{
		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
			}
		}

		void init()
		{
		}

		void update()
		{
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "GAME DEVELOPER";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 8;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;
			int spacing = font + 10;
			float spacingText = 2.0f;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			text = "MAURO MENDOZA";
			posY += spacing;
			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			font = game_settings::fontOption - 5;
			posY = (game::screenHeight * 3 / 8);
			text = "Soundtracks: Creative Commons Zero 1.0";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			spacing = font + 10;
			posY += spacing;
			text = "Sounds effects: licensing.jamendo.com";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			posY += spacing;
			text = "Texture Paddle: Vaus Game";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			posY += spacing;
			text = "Texture Ball: SDL BALL Game";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			posY += spacing;
			text = "Texture Blocks: Magma MK II";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);
			
			posY += spacing;
			text = "Texture Bomb: cleanpng.com";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			posY += spacing;
			text = "Bomb explosion animation: vectorstock.com";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			posY += spacing;
			text = "Backgrounds: pngtree.com";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			posY += spacing;
			text = "Fonts: 1001freefonts.com";
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, color);

			font = game_settings::fontOption;
			text = "PRESS ENTER FOR BACK TO MENU";
			posY = game::screenHeight * 7 / 8;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, fonts::optionSelected);
		}

		void deInit()
		{
		}
	}
}