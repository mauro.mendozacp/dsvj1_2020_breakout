#pragma once
#ifndef FINISH_GAME_H
#define FINISH_GAME_H

#include "raylib.h"

namespace breakout
{
	namespace finish_game
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !FINISH_GAME_H