#include "finish_game.h"
#include "game/game.h"

namespace breakout
{
	namespace finish_game
	{
		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				gameplay::deInit();
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
			}
		}

		void init()
		{
			if (gameplay::player->getScore() > game::highScore)
			{
				SaveStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE, gameplay::player->getScore());
			}
		}

		void update()
		{
			acceptOption();
		}

		void draw()
		{
			//Title
			const char* text = "YOU FINISH GAME!!!";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//HighScore
			if (gameplay::player->getScore() > game::highScore)
			{
				text = TextFormat("NEW HIGHSCORE: %i", gameplay::player->getScore());
			}
			else
			{
				text = TextFormat("YOU SCORE: %i", gameplay::player->getScore());
			}
			font = game_settings::fontOption + 5;
			posY += font + 15;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;
			color = fonts::optionSelected;

			text = "BACK TO MENU";
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);
		}

		void deInit()
		{
			game::highScore = LoadStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE);
		}
	}
}