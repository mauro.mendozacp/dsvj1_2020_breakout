#include "options.h"
#include "game/game.h"

namespace breakout
{
	namespace options
	{
		enum class OPTIONS_MENU
		{
			GAME_SETTINGS = 1,
			GAMEPLAY_SETTINGS,
			CONTROLS,
			GO_BACK
		};

		OPTIONS_MENU optionsMenu;

		static OPTIONS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<OPTIONS_MENU>(static_cast<int>(optionsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<OPTIONS_MENU>(static_cast<int>(optionsMenu) + 1);
			}

			return optionsMenu;
		}

		static bool checkOption(OPTIONS_MENU auxOption)
		{
			switch (auxOption)
			{
			case OPTIONS_MENU::GAME_SETTINGS:
			case OPTIONS_MENU::GAMEPLAY_SETTINGS:
			case OPTIONS_MENU::CONTROLS:
			case OPTIONS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				OPTIONS_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					optionsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (optionsMenu)
				{
				case OPTIONS_MENU::GAME_SETTINGS:
					game::changeStatus(game::GAME_STATUS::GAME_SETTINGS);
					break;
				case OPTIONS_MENU::GAMEPLAY_SETTINGS:
					game::changeStatus(game::GAME_STATUS::GAMEPLAY_SETTINGS);
					break;
				case OPTIONS_MENU::CONTROLS:
					game::changeStatus(game::GAME_STATUS::CONTROLS);
					break;
				case OPTIONS_MENU::GO_BACK:
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(OPTIONS_MENU option)
		{
			if (option == optionsMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		void init()
		{
			optionsMenu = OPTIONS_MENU::GAME_SETTINGS;
		}

		void update()
		{
			moveOption();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "OPTIONS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "GAME SETTINGS";
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(OPTIONS_MENU::GAME_SETTINGS));

			text = "GAMEPLAY SETTINGS";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(OPTIONS_MENU::GAMEPLAY_SETTINGS));

			text = "CONTROLS";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(OPTIONS_MENU::CONTROLS));

			text = "BACK TO MENU";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(OPTIONS_MENU::GO_BACK));
		}

		void deInit()
		{
		}
	}
}