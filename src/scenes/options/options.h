#pragma once
#ifndef OPTIONS_H
#define OPTIONS_H

#include "raylib.h"

namespace breakout
{
	namespace options
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !OPTIONS_H