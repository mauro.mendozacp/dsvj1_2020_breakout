#pragma once
#ifndef GAME_SETTINGS_H
#define GAME_SETTINGS_H

#include "raylib.h"

namespace breakout
{
	namespace game_settings
	{
		extern int fontTitle;
		extern int fontOption;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GAME_SETTINGS_H