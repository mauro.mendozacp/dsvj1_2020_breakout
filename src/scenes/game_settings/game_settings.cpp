#include "game_settings.h"
#include "game/game.h"

namespace breakout
{
	namespace game_settings
	{
		const int resolutionLenght = 3;
		Vector2 resolutionValues[resolutionLenght] = { { 400, 600 }, { 840, 650 }, { 840, 900 } };
		const char* resolutionsText[resolutionLenght] = { "400x600", "840x650", "840x900" };
		int fontTitleValues[resolutionLenght] = { 35, 55, 75 };
		int fontOptionValues[resolutionLenght] = { 17, 25, 35 };

		const int volumeLenght = 6;
		float volumeValues[volumeLenght] = { 0, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f };
		float musicVolume = volumeValues[3];
		float sfxVolume = volumeValues[4];

		Vector2 resolution = resolutionValues[1];
		int fontTitle = fontTitleValues[1];
		int fontOption = fontOptionValues[1];

		enum class GAME_SETTINGS_MENU
		{
			RESOLUTION = 1,
			MUSIC,
			SFX,
			GO_BACK
		};

		GAME_SETTINGS_MENU gameSettingsMenu;

		static int getNewValue(int currentValue, int minValue, int maxValue)
		{
			int auxValue = 0;

			if (input::checkMoveLeftOption())
			{
				auxValue = currentValue - 1;
				if (auxValue >= minValue)
				{
					return auxValue;
				}
				else
				{
					return currentValue;
				}
			}
			if (input::checkMoveRightOption())
			{
				auxValue = currentValue + 1;
				if (auxValue <= maxValue)
				{
					return auxValue;
				}
				else
				{
					return currentValue;
				}
			}

			return currentValue;
		}

		static int getIndexResolutionValue(float width, float height)
		{
			for (int i = 0; i < resolutionLenght; i++)
			{
				if (width == resolutionValues[i].x && height == resolutionValues[i].y)
				{
					return i;
				}
			}

			return -1;
		}

		static int getIndexVolumeValue(float volume)
		{
			for (int i = 0; i < volumeLenght; i++)
			{
				if (volume == volumeValues[i])
				{
					return i;
				}
			}

			return -1;
		}

		static void changeValue()
		{
			if (input::checkMoveLeftRightOption())
			{
				switch (gameSettingsMenu)
				{
				case GAME_SETTINGS_MENU::RESOLUTION:
					resolution = resolutionValues[getNewValue(getIndexResolutionValue(resolution.x, resolution.y), 0, (resolutionLenght - 1))];
					break;
				case GAME_SETTINGS_MENU::MUSIC:
					musicVolume = volumeValues[getNewValue(getIndexVolumeValue(musicVolume), 0, (volumeLenght - 1))];
					audio::setMusicVolume(musicVolume);
					break;
				case GAME_SETTINGS_MENU::SFX:
					sfxVolume = volumeValues[getNewValue(getIndexVolumeValue(sfxVolume), 0, (volumeLenght - 1))];
					audio::setSfxVolume(sfxVolume);
					break;
				default:
					break;
				}
			}
		}

		static GAME_SETTINGS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<GAME_SETTINGS_MENU>(static_cast<int>(gameSettingsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<GAME_SETTINGS_MENU>(static_cast<int>(gameSettingsMenu) + 1);
			}

			return gameSettingsMenu;
		}

		static bool checkOption(GAME_SETTINGS_MENU auxOption)
		{
			switch (auxOption)
			{
			case GAME_SETTINGS_MENU::RESOLUTION:
			case GAME_SETTINGS_MENU::MUSIC:
			case GAME_SETTINGS_MENU::SFX:
			case GAME_SETTINGS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				GAME_SETTINGS_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					gameSettingsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				if (gameSettingsMenu == GAME_SETTINGS_MENU::GO_BACK)
				{
					game::changeStatus(game::GAME_STATUS::OPTIONS);
				}
			}
		}

		static Color getColorOption(GAME_SETTINGS_MENU option)
		{
			if (option == gameSettingsMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		static void updateGameValues()
		{
			player::playerStartPosition = { ((static_cast<float>(game::screenWidth) / 2) - (player::playerPaddleWidth / 2)), (static_cast<float>(game::screenHeight) * 7 / 8) };
			block::blockPaddleWidth = static_cast<float>((game::screenWidth / gameplay::blockCol));
		}

		void init()
		{
			gameSettingsMenu = GAME_SETTINGS_MENU::RESOLUTION;

			resolution = { static_cast<float>(game::screenWidth), static_cast<float>(game::screenHeight) };
			musicVolume = audio::musicVolume;
			sfxVolume = audio::sfxVolume;
		}

		void update()
		{
			moveOption();
			changeValue();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "GAME SETTINGS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//Options
			font = fontOption;
			int spacing = font + 10;

			text = "RESOLUTION";
			posX = game::screenWidth / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAME_SETTINGS_MENU::RESOLUTION));

			text = "MUSIC";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAME_SETTINGS_MENU::MUSIC));

			text = "SFX";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAME_SETTINGS_MENU::SFX));

			text = "GO BACK";
			posX = game::screenWidth / 2;
			posY = game::screenHeight * 3 / 4;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAME_SETTINGS_MENU::GO_BACK));

			//Values
			font = fontOption - 3;
			posX = game::screenWidth * 3 / 4;

			text = resolutionsText[getIndexResolutionValue(resolution.x, resolution.y)];
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAME_SETTINGS_MENU::RESOLUTION));

			posY += spacing;
			float fposX = static_cast<float>(posX);
			float fposY = static_cast<float>((posY - (font / 2)));
			float ffont = static_cast<float>(font);

			Rectangle graphicVolume[volumeLenght - 1];

			graphicVolume[0] = { fposX - ffont * 2 - ffont / 2, fposY + ffont * 3 / 4, ffont, (ffont / 4) };
			graphicVolume[1] = { fposX - ffont - ffont / 2, fposY + ffont / 2, ffont, (ffont / 2) };
			graphicVolume[2] = { fposX - ffont / 2, fposY + ffont / 4, ffont, (ffont - ffont / 4) };
			graphicVolume[3] = { fposX + ffont - ffont / 2, fposY, ffont, (ffont) };
			graphicVolume[4] = { fposX + ffont * 2 - ffont / 2, fposY - ffont / 4, ffont, (ffont + ffont / 4) };

			if (getIndexVolumeValue(musicVolume) != 0)
			{
				for (int i = 0; i <= getIndexVolumeValue(musicVolume) - 1; i++)
				{
					DrawRectangle(static_cast<int>(graphicVolume[i].x), static_cast<int>(graphicVolume[i].y),
						static_cast<int>(graphicVolume[i].width), static_cast<int>(graphicVolume[i].height),
						getColorOption(GAME_SETTINGS_MENU::MUSIC));
				}
			}

			posY += spacing;
			fposX = static_cast<float>(posX);
			fposY = static_cast<float>((posY - (font / 2)));
			ffont = static_cast<float>(font);

			graphicVolume[0] = { fposX - ffont * 2 - ffont / 2, fposY + ffont * 3 / 4, ffont, (ffont / 4) };
			graphicVolume[1] = { fposX - ffont - ffont / 2, fposY + ffont / 2, ffont, (ffont / 2) };
			graphicVolume[2] = { fposX - ffont / 2, fposY + ffont / 4, ffont, (ffont - ffont / 4) };
			graphicVolume[3] = { fposX + ffont - ffont / 2, fposY, ffont, (ffont) };
			graphicVolume[4] = { fposX + ffont * 2 - ffont / 2, fposY - ffont / 4, ffont, (ffont + ffont / 4) };

			if (getIndexVolumeValue(sfxVolume) != 0)
			{
				for (int i = 0; i <= getIndexVolumeValue(sfxVolume) - 1; i++)
				{
					DrawRectangle(static_cast<int>(graphicVolume[i].x), static_cast<int>(graphicVolume[i].y),
						static_cast<int>(graphicVolume[i].width), static_cast<int>(graphicVolume[i].height),
						getColorOption(GAME_SETTINGS_MENU::SFX));
				}
			}

			//Arrow decrease
			font = fontOption;

			posX = (game::screenWidth * 3 / 4) - font - 35;
			text = "<";

			posY = game::screenHeight / 2;
			if (getIndexResolutionValue(resolution.x, resolution.y) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexVolumeValue(musicVolume) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexVolumeValue(sfxVolume) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			//Arrow increase
			posX = (game::screenWidth * 3 / 4) + font + 35;
			text = ">";

			posY = game::screenHeight / 2;
			if (getIndexResolutionValue(resolution.x, resolution.y) < (resolutionLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexVolumeValue(musicVolume) < (volumeLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexVolumeValue(sfxVolume) < (volumeLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}
		}

		void deInit()
		{
			if (resolution.x != static_cast<float>(game::screenWidth) || resolution.y != static_cast<float>(game::screenHeight))
			{
				texture::deInit();
				CloseWindow();

				game::screenWidth = static_cast<int>(resolution.x);
				game::screenHeight = static_cast<int>(resolution.y);

				updateGameValues();
				
				fontTitle = fontTitleValues[getIndexResolutionValue(resolution.x, resolution.y)];
				fontOption = fontOptionValues[getIndexResolutionValue(resolution.x, resolution.y)];

				InitWindow(game::screenWidth, game::screenHeight, game::titleGame);
				texture::init();
				fonts::init();
			}
		}
	}
}