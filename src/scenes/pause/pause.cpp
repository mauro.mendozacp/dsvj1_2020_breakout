#include "pause.h"
#include "game/game.h"

namespace breakout
{
	namespace pause
	{
		enum class PAUSE_MENU
		{
			CONTINUE = 1,
			GO_BACK
		};

		PAUSE_MENU pauseMenu;

		static PAUSE_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<PAUSE_MENU>(static_cast<int>(pauseMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<PAUSE_MENU>(static_cast<int>(pauseMenu) + 1);
			}

			return pauseMenu;
		}

		static bool checkOption(PAUSE_MENU auxOption)
		{
			switch (auxOption)
			{
			case PAUSE_MENU::CONTINUE:
			case PAUSE_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				PAUSE_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					pauseMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (pauseMenu)
				{
				case PAUSE_MENU::CONTINUE:
					ResumeMusicStream(audio::gameplayMusic);
					game::gameStatus = game::GAME_STATUS::INGAME;
					deInit();
					break;
				case PAUSE_MENU::GO_BACK:
					gameplay::deInit();
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(PAUSE_MENU option)
		{
			if (option == pauseMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		void init()
		{
			pauseMenu = PAUSE_MENU::CONTINUE;
			PauseMusicStream(audio::gameplayMusic);
		}

		void update()
		{
			moveOption();
			acceptOption();
		}

		void draw()
		{
			//Title
			const char* text = "PAUSE";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "CONTINUE";
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(PAUSE_MENU::CONTINUE));

			text = "BACK TO MENU";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(PAUSE_MENU::GO_BACK));
		}

		void deInit()
		{
		}
	}
}