#pragma once
#ifndef PAUSE_H
#define PAUSE_H

#include "raylib.h"

namespace breakout
{
	namespace pause
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !PAUSE_H
