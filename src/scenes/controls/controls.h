#pragma once
#ifndef CONTROLS_H
#define CONTROLS_H

#include "raylib.h"

namespace breakout
{
	namespace controls
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !CONTROLS_H
