#include "controls.h"
#include "game/game.h"

namespace breakout
{
	namespace controls
	{
		enum class CONTROLS_MENU
		{
			MOVE_LEFT_PLAYER = 1,
			MOVE_RIGHT_PLAYER,
			SHOOT_BALL,
			PAUSE_GAME,
			GO_BACK
		};

		CONTROLS_MENU controlsMenu;
		bool wait;

		static CONTROLS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<CONTROLS_MENU>(static_cast<int>(controlsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<CONTROLS_MENU>(static_cast<int>(controlsMenu) + 1);
			}

			return controlsMenu;
		}

		static bool checkMainMenuOption(CONTROLS_MENU auxOption)
		{
			switch (auxOption)
			{
			case CONTROLS_MENU::MOVE_LEFT_PLAYER:
			case CONTROLS_MENU::MOVE_RIGHT_PLAYER:
			case CONTROLS_MENU::SHOOT_BALL:
			case CONTROLS_MENU::PAUSE_GAME:
			case CONTROLS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption() && !wait)
			{
				CONTROLS_MENU auxOption = getNewOptionMenu();

				if (checkMainMenuOption(auxOption))
				{
					controlsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void changeInput()
		{
			if (wait)
			{
				int inputKey = input::GetInputChange();

				switch (controlsMenu)
				{
				case CONTROLS_MENU::MOVE_LEFT_PLAYER:
					input::moveLeftPlayer = inputKey;
					break;
				case CONTROLS_MENU::MOVE_RIGHT_PLAYER:
					input::moveRightPlayer = inputKey;
					break;
				case CONTROLS_MENU::SHOOT_BALL:
					input::shootBall = inputKey;
					break;
				case CONTROLS_MENU::PAUSE_GAME:
					input::pauseGame = inputKey;
					break;
				default:
					break;
				}

				if (inputKey != 0)
				{
					wait = false;
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				if (controlsMenu == CONTROLS_MENU::GO_BACK)
				{
					game::changeStatus(game::GAME_STATUS::OPTIONS);
				}
				else
				{
					wait = true;
				}
			}
		}

		static Color getColorOption(CONTROLS_MENU option)
		{
			if (option == controlsMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		void init()
		{
			controlsMenu = CONTROLS_MENU::MOVE_LEFT_PLAYER;
			wait = false;
		}

		void update()
		{
			moveOption();
			acceptOption();
			changeInput();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "CONTROLS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "MOVE LEFT PLAYER";
			posX = game::screenWidth / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::MOVE_LEFT_PLAYER));

			text = "MOVE RIGHT PLAYER";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::MOVE_RIGHT_PLAYER));

			text = "SHOOT BALL";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::SHOOT_BALL));

			text = "PAUSE GAME";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::PAUSE_GAME));

			text = "GO BACK";
			posX = game::screenWidth / 2;
			posY = game::screenHeight * 3 / 4;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::GO_BACK));

			//Controls
			posX = game::screenWidth * 3 / 4;

			text = input::GetInputText(input::moveLeftPlayer);
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::MOVE_LEFT_PLAYER));

			text = input::GetInputText(input::moveRightPlayer);
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::MOVE_RIGHT_PLAYER));

			text = input::GetInputText(input::shootBall);
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::SHOOT_BALL));

			text = input::GetInputText(input::pauseGame);
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(CONTROLS_MENU::PAUSE_GAME));
		}

		void deInit()
		{
		}
	}
}