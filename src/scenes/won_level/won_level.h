#pragma once
#ifndef WON_LEVEL_H
#define WON_LEVEL_H

#include "raylib.h"

namespace breakout
{
	namespace won_level
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !WON_LEVEL_H
