#include "won_level.h"
#include "game/game.h"

namespace breakout
{
	namespace won_level
	{
		enum class WON_LEVEL_MENU
		{
			NEXT_LEVEL = 1,
			GO_BACK
		};

		WON_LEVEL_MENU wonLevelMenu;

		static WON_LEVEL_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<WON_LEVEL_MENU>(static_cast<int>(wonLevelMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<WON_LEVEL_MENU>(static_cast<int>(wonLevelMenu) + 1);
			}

			return wonLevelMenu;
		}

		static bool checkOption(WON_LEVEL_MENU auxOption)
		{
			switch (auxOption)
			{
			case WON_LEVEL_MENU::NEXT_LEVEL:
			case WON_LEVEL_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				WON_LEVEL_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					wonLevelMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (wonLevelMenu)
				{
				case WON_LEVEL_MENU::NEXT_LEVEL:
					PlayMusicStream(audio::gameplayMusic);
					game::gameStatus = game::GAME_STATUS::INGAME;
					deInit();
					levels::nextLevel();
					gameplay::startRound();
					break;
				case WON_LEVEL_MENU::GO_BACK:
					gameplay::deInit();
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(WON_LEVEL_MENU option)
		{
			if (option == wonLevelMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		void init()
		{
			wonLevelMenu = WON_LEVEL_MENU::NEXT_LEVEL;
			StopMusicStream(audio::gameplayMusic);
		}

		void update()
		{
			moveOption();
			acceptOption();
		}

		void draw()
		{
			//Title
			const char* text = "GREAT!!!";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "NEXT LEVEL";
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(WON_LEVEL_MENU::NEXT_LEVEL));

			text = "BACK TO MENU";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(WON_LEVEL_MENU::GO_BACK));
		}

		void deInit()
		{
		}
	}
}