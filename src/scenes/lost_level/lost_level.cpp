#include "lost_level.h"
#include "game/game.h"

namespace breakout
{
	namespace lost_level
	{
		enum class LOST_LEVEL_MENU
		{
			RESTART = 1,
			GO_BACK,
		};

		LOST_LEVEL_MENU lostLevelMenu;

		static LOST_LEVEL_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<LOST_LEVEL_MENU>(static_cast<int>(lostLevelMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<LOST_LEVEL_MENU>(static_cast<int>(lostLevelMenu) + 1);
			}

			return lostLevelMenu;
		}

		static bool checkOption(LOST_LEVEL_MENU auxOption)
		{
			switch (auxOption)
			{
			case LOST_LEVEL_MENU::RESTART:
			case LOST_LEVEL_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				LOST_LEVEL_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					lostLevelMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (lostLevelMenu)
				{
				case LOST_LEVEL_MENU::RESTART:
					game::changeStatus(game::GAME_STATUS::INGAME);
					break;
				case LOST_LEVEL_MENU::GO_BACK:
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(LOST_LEVEL_MENU option)
		{
			if (option == lostLevelMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		void init()
		{
			lostLevelMenu = LOST_LEVEL_MENU::RESTART;
			StopMusicStream(audio::gameplayMusic);

			if (gameplay::player->getScore() > game::highScore)
			{
				SaveStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE, gameplay::player->getScore());
			}
		}

		void update()
		{
			moveOption();
			acceptOption();
		}

		void draw()
		{
			//Title
			const char* text = "YOU LOST";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//HighScore
			if (gameplay::player->getScore() > game::highScore)
			{
				text = TextFormat("NEW HIGHSCORE: %i", gameplay::player->getScore());
			}
			else
			{
				text = TextFormat("YOU SCORE: %i", gameplay::player->getScore());
			}
			font = game_settings::fontOption + 5;
			posY += font + 15;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "RESTART";
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(LOST_LEVEL_MENU::RESTART));

			text = "BACK TO MENU";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(LOST_LEVEL_MENU::GO_BACK));
		}

		void deInit()
		{
			game::highScore = LoadStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE);
		}
	}
}