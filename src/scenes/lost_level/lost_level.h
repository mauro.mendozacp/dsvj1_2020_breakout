#pragma once
#ifndef LOST_LEVEL_H
#define LOST_LEVEL_H

#include "raylib.h"

namespace breakout
{
	namespace lost_level
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !LOST_LEVEL_H