#include "main_menu.h"
#include "game/game.h"

namespace breakout
{
	namespace main_menu
	{
		enum class MAIN_MENU
		{
			PLAY = 1,
			OPTIONS,
			CREDITS,
			EXIT
		};

		MAIN_MENU mainMenu;

		static MAIN_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<MAIN_MENU>(static_cast<int>(mainMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<MAIN_MENU>(static_cast<int>(mainMenu) + 1);
			}

			return mainMenu;
		}

		static bool checkOption(MAIN_MENU auxOption)
		{
			switch (auxOption)
			{
			case MAIN_MENU::PLAY:
			case MAIN_MENU::OPTIONS:
			case MAIN_MENU::CREDITS:
			case MAIN_MENU::EXIT:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				MAIN_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					mainMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (mainMenu)
				{
				case MAIN_MENU::PLAY:
					StopMusicStream(audio::menuMusic);
					game::changeStatus(game::GAME_STATUS::INGAME);
					break;
				case MAIN_MENU::OPTIONS:
					game::changeStatus(game::GAME_STATUS::OPTIONS);
					break;
				case MAIN_MENU::CREDITS:
					game::changeStatus(game::GAME_STATUS::CREDITS);
					break;
				case MAIN_MENU::EXIT:
					game::changeStatus(game::GAME_STATUS::EXIT);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(MAIN_MENU option)
		{
			if (option == mainMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}



		void init()
		{
			mainMenu = MAIN_MENU::PLAY;
			PlayMusicStream(audio::menuMusic);
		}

		void update()
		{
			moveOption();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			//Title
			const char* text = "BREAKOUT";
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//HighScore
			if (game::highScore > 0)
			{
				text = TextFormat("HIGHSCORE: %i", game::highScore);
				font = game_settings::fontOption;
				posY += font + 15;

				fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);
			}

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "PLAY";
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(MAIN_MENU::PLAY));

			text = "OPTIONS";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(MAIN_MENU::OPTIONS));

			text = "CREDITS";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(MAIN_MENU::CREDITS));

			text = "EXIT";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(MAIN_MENU::EXIT));
		}

		void deInit()
		{
		}
	}
}