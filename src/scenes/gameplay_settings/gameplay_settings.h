#pragma once
#ifndef GAMEPLAY_SETTINGS_H
#define GAMEPLAY_SETTINGS_H

#include "raylib.h"

namespace breakout
{
	namespace gameplay_settings
	{
		extern float playerSpeed;
		extern float ballSpeed;
		extern int playerStartLifes;
		extern int chancePowerup;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GAMEPLAY_SETTINGS_H