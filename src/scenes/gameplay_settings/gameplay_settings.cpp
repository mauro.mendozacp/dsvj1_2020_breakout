#include "gameplay_settings.h"
#include "game/game.h"

namespace breakout
{
	namespace gameplay_settings
	{
		enum class GAMEPLAY_SETTINGS_MENU
		{
			PLAYER_SPEED = 1,
			BALL_SPEED,
			PLAYER_START_LIVES,
			CHANGE_POWERUP,
			GO_BACK
		};

		GAMEPLAY_SETTINGS_MENU gameplaySettingsMenu;

		const int playerStartLifesMax = 15;
		const int playerStartLifesMin = 1;

		const int speedLenght = 5;
		const char* speedText[speedLenght] = { "VERY SLOW", "SLOW", "NORMAL", "FAST", "VERY FAST" };
		float speedValues[speedLenght] = { 100.0f, 200.0f, 350.0f, 500.0f, 600.0f };

		const int chanceLenght = 3;
		const char* chanceText[chanceLenght] = { "LOW", "MEDIUM", "HIGH" };
		int chanceValues[chanceLenght] = { 25, 50, 75 };

		float playerSpeed = speedValues[(speedLenght / 2)];
		float ballSpeed = speedValues[(speedLenght / 2)];
		int playerStartLifes = 3;
		int chancePowerup = chanceValues[(chanceLenght / 2)];

		static int getNewValue(int currentValue, int minValue, int maxValue)
		{
			int auxValue = 0;

			if (input::checkMoveLeftOption())
			{
				auxValue = currentValue - 1;
				if (auxValue >= minValue)
				{
					return auxValue;
				}
				else
				{
					return currentValue;
				}
			}
			if (input::checkMoveRightOption())
			{
				auxValue = currentValue + 1;
				if (auxValue <= maxValue)
				{
					return auxValue;
				}
				else
				{
					return currentValue;
				}
			}

			return currentValue;
		}

		int getIndexSpeedValue(float speed)
		{
			for (int i = 0; i < speedLenght; i++)
			{
				if (speed == speedValues[i])
				{
					return i;
				}
			}

			return -1;
		}

		int getIndexChanceValue(int chance)
		{
			for (int i = 0; i < chanceLenght; i++)
			{
				if (chance == chanceValues[i])
				{
					return i;
				}
			}

			return -1;
		}

		static void changeValue()
		{
			if (input::checkMoveLeftRightOption())
			{
				switch (gameplaySettingsMenu)
				{
				case GAMEPLAY_SETTINGS_MENU::PLAYER_SPEED:
					playerSpeed = speedValues[(getNewValue(getIndexSpeedValue(playerSpeed), 0, (speedLenght - 1)))];
					break;
				case GAMEPLAY_SETTINGS_MENU::BALL_SPEED:
					ballSpeed = speedValues[(getNewValue(getIndexSpeedValue(ballSpeed), 0, (speedLenght - 1)))];
					break;
				case GAMEPLAY_SETTINGS_MENU::PLAYER_START_LIVES:
					playerStartLifes = getNewValue(playerStartLifes, playerStartLifesMin, playerStartLifesMax);
					break;
				case GAMEPLAY_SETTINGS_MENU::CHANGE_POWERUP:
					chancePowerup = chanceValues[(getNewValue(getIndexChanceValue(chancePowerup), 0, (chanceLenght - 1)))];
					break;
				default:
					break;
				}
			}
		}

		static GAMEPLAY_SETTINGS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<GAMEPLAY_SETTINGS_MENU>(static_cast<int>(gameplaySettingsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<GAMEPLAY_SETTINGS_MENU>(static_cast<int>(gameplaySettingsMenu) + 1);
			}

			return gameplaySettingsMenu;
		}

		static bool checkOption(GAMEPLAY_SETTINGS_MENU auxOption)
		{
			switch (auxOption)
			{
			case GAMEPLAY_SETTINGS_MENU::PLAYER_SPEED:
			case GAMEPLAY_SETTINGS_MENU::BALL_SPEED:
			case GAMEPLAY_SETTINGS_MENU::PLAYER_START_LIVES:
			case GAMEPLAY_SETTINGS_MENU::CHANGE_POWERUP:
			case GAMEPLAY_SETTINGS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				GAMEPLAY_SETTINGS_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					gameplaySettingsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				if (gameplaySettingsMenu == GAMEPLAY_SETTINGS_MENU::GO_BACK)
				{
					game::changeStatus(game::GAME_STATUS::OPTIONS);
				}
			}
		}

		static Color getColorOption(GAMEPLAY_SETTINGS_MENU option)
		{
			if (option == gameplaySettingsMenu)
			{
				return fonts::optionSelected;
			}

			return fonts::optionNoSelected;
		}

		void init()
		{
			gameplaySettingsMenu = GAMEPLAY_SETTINGS_MENU::PLAYER_SPEED;
		}

		void update()
		{
			moveOption();
			changeValue();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "GAMEPLAY SETTINGS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = game_settings::fontTitle;
			Color color = fonts::optionNoSelected;

			fonts::drawCenterText(fonts::title, text, posX, posY, font, fonts::titleSpacing, color);

			//Options
			font = game_settings::fontOption;
			int spacing = font + 10;

			text = "PLAYER SPEED";
			posX = game::screenWidth / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAMEPLAY_SETTINGS_MENU::PLAYER_SPEED));

			text = "BALL SPEED";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAMEPLAY_SETTINGS_MENU::BALL_SPEED));

			text = "PLAYER START LIFES";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAMEPLAY_SETTINGS_MENU::PLAYER_START_LIVES));

			text = "CHANCE POWERUP";
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAMEPLAY_SETTINGS_MENU::CHANGE_POWERUP));

			text = "GO BACK";
			posX = game::screenWidth / 2;
			posY = game::screenHeight * 3 / 4;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, getColorOption(GAMEPLAY_SETTINGS_MENU::GO_BACK));

			//Values
			font = game_settings::fontOption - 3;
			posX = game::screenWidth * 3 / 4;
			float spacingText = fonts::optionSpacing - 3;

			text = speedText[getIndexSpeedValue(playerSpeed)];
			posY = game::screenHeight / 2;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, getColorOption(GAMEPLAY_SETTINGS_MENU::PLAYER_SPEED));

			text = speedText[getIndexSpeedValue(ballSpeed)];
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, getColorOption(GAMEPLAY_SETTINGS_MENU::BALL_SPEED));

			text = TextFormat("%i", playerStartLifes);
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, getColorOption(GAMEPLAY_SETTINGS_MENU::PLAYER_START_LIVES));

			text = chanceText[getIndexChanceValue(chancePowerup)];
			posY += spacing;
			fonts::drawCenterText(fonts::option, text, posX, posY, font, spacingText, getColorOption(GAMEPLAY_SETTINGS_MENU::CHANGE_POWERUP));

			//Arrow decrease
			font = game_settings::fontOption;
			color = LIGHTGRAY;

			posX = (game::screenWidth * 3 / 4) - font - 35;
			text = "<";

			posY = game::screenHeight / 2;
			if (getIndexSpeedValue(playerSpeed) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexSpeedValue(ballSpeed) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (playerStartLifes > playerStartLifesMin)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexChanceValue(chancePowerup) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			//Arrow increase
			color = LIGHTGRAY;

			posX = (game::screenWidth * 3 / 4) + font + 35;
			text = ">";

			posY = game::screenHeight / 2;
			if (getIndexSpeedValue(playerSpeed) < (speedLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexSpeedValue(ballSpeed) < (speedLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (playerStartLifes < playerStartLifesMax)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}

			posY += spacing;
			if (getIndexChanceValue(chancePowerup) < (chanceLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}
		}

		void deInit()
		{
		}
	}
}