#include "gameplay.h"
#include "game/game.h"

namespace breakout
{
	namespace gameplay
	{
		Vector2 endPositionLine;
		Vector2 secPositionLine;

		player::Player* player;
		const int ballLenght = 3;
		ball::Ball* ball[ballLenght];
		const int blockRow = 10;
		const int blockCol = 12;
		const int blockLength = blockRow * blockCol;
		block::Block* blocks[blockLength];
		powerup::Powerup* powerup;

		bool startGame;
		int levelGame;
		float lineLarge = 200;

		static bool finihLevel()
		{
			for (int i = 0; i < blockLength; i++)
			{
				if (!blocks[i]->isBlockEmpty())
				{
					return false;
				}
			}

			return true;
		}

		static void setEndPositionLine(float limit)
		{
			endPositionLine = ball::getVelocityForAngle(ball[0]->getAngle(), limit);
			if (ball[0]->getPosition().x < (game::screenWidth / 2))
			{
				endPositionLine.x *= -1;
			}
			endPositionLine.y *= -1;

			endPositionLine = { (ball[0]->getPosition().x + endPositionLine.x), (ball[0]->getPosition().y + endPositionLine.y) };
		}

		static void setSecPositionLine()
		{
			float lineCut = 0;

			if (endPositionLine.x <= 0)
			{
				endPositionLine.x = 0;
				endPositionLine.y = ball[0]->getPosition().y - (ball[0]->getPosition().x * static_cast<float>(tan(static_cast<double>(ball[0]->getAngle()) * PI / 180)));

				lineCut = lineLarge - ((ball[0]->getPosition().y - endPositionLine.y) / static_cast<float>(sin(static_cast<double>(ball[0]->getAngle()) * PI / 180)));
				secPositionLine = ball::getVelocityForAngle(ball[0]->getAngle(), lineCut);
				secPositionLine.y = endPositionLine.y - secPositionLine.y;
			}
			else
			{
				endPositionLine.x = static_cast<float>(game::screenWidth);
				endPositionLine.y = ball[0]->getPosition().y - ((static_cast<float>(game::screenWidth) - ball[0]->getPosition().x) * static_cast<float>(tan(static_cast<double>(ball[0]->getAngle()) * PI / 180)));

				lineCut = lineLarge - ((ball[0]->getPosition().y - endPositionLine.y) / static_cast<float>(sin(static_cast<double>(ball[0]->getAngle()) * PI / 180)));
				secPositionLine = ball::getVelocityForAngle(ball[0]->getAngle(), lineCut);
				secPositionLine.y = endPositionLine.y - secPositionLine.y;
				secPositionLine.x = static_cast<float>(game::screenWidth) - secPositionLine.x;
			}
		}

		static void showHUD()
		{
			//Title
			const char* text = FormatText("LEVEL: %i", levelGame);
			int font = game_settings::fontOption;
			int posX = game::screenWidth / 16;
			int posY = font + 10;
			Color color = fonts::optionNoSelected;

			if (posX - (MeasureTextEx(fonts::option, text, static_cast<float>(font), fonts::optionSpacing).x) / 2 < 0)
			{
				Vector2 pos;
				pos.x = static_cast<float>(font);
				pos.y = static_cast<float>((posY - (font / 2)));
				DrawTextEx(fonts::option, text, pos, static_cast<float>(font), fonts::optionSpacing, color);
			}
			else
			{
				fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);
			}

			text = FormatText("SCORE: %i", player->getScore());
			posX = game::screenWidth / 2;

			fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);

			text = FormatText("LIFES: %i", player->getLife());
			posX = game::screenWidth * 15 / 16;

			if (posX + (MeasureTextEx(fonts::option, text, static_cast<float>(font), fonts::optionSpacing).x) / 2 > game::screenWidth)
			{
				Vector2 pos;
				pos.x = static_cast<float>(game::screenWidth - MeasureTextEx(fonts::option, text, static_cast<float>(font), fonts::optionSpacing).x - font);
				pos.y = static_cast<float>((posY - (font / 2)));
				DrawTextEx(fonts::option, text, pos, static_cast<float>(font), fonts::optionSpacing, color);
			}
			else
			{
				fonts::drawCenterText(fonts::option, text, posX, posY, font, fonts::optionSpacing, color);
			}
		}

		void startRound()
		{
			player->reset();
			ball::reset();
			setEndPositionLine(lineLarge);
			startGame = false;
		}

		bool lostRound()
		{
			for (int i = 0; i < ballLenght; i++)
			{
				if (ball[i] != NULL)
				{
					return false;
				}
			}

			return true;
		}

		void init()
		{
			startGame = false;

			player::init();
			ball::init();
			block::init();
			powerup = NULL;
			//texture::setSizeTexture();

			setEndPositionLine(lineLarge);
			levels::startLevelGame();
			PlayMusicStream(audio::gameplayMusic);
		}

		void update()
		{
			UpdateMusicStream(audio::gameplayMusic);
			player->move();
			
			if (startGame)
			{
				for (int i = 0; i < ballLenght; i++)
				{
					if (ball[i] != NULL)
					{
						ball[i]->move();
						ball[i]->collisionLimit();
						ball[i]->collisionPlayer(player->getRec());
					}
				}

				int blocksCollisionated = 0;
				bool multiCollision = false;
				//Collision blocks
				for (int i = 0; i < blockLength; i++)
				{
					if (!blocks[i]->isBlockEmpty())
					{
						for (int k = 0; k < ballLenght; k++)
						{
							if (ball[k] != NULL)
							{
								if (ball[k]->checkCollisionPaddle(blocks[i]->getRec()))
								{
									//Check Collision multi blocks
									for (int j = 0; j < blockLength; j++)
									{
										if (!blocks[j]->isBlockEmpty())
										{
											for (int l = 0; l < ballLenght; l++)
											{
												if (ball[l] != NULL)
												{
													if (ball[l]->checkCollisionPaddle(blocks[j]->getRec()))
													{
														if (!blocks[j]->isBlockUnbreakable())
														{
															if (ball[l]->getFireball())
															{
																blocks[j]->setLife(0);
															}
															else
															{
																blocks[j]->setLife(blocks[j]->getLife() - 1);
															}

															if (blocks[j]->getLife() <= 0)
															{
																block::destroyBlock(j);
															}
														}

														blocksCollisionated++;
													}
												}
											}
										}
									}

									PlaySound(audio::popSound);

									if (blocksCollisionated > 1)
									{
										multiCollision = true;
									}
									
									if (!ball[k]->getFireball())
									{
										ball[k]->collisionBlock(blocks[i]->getRec(), multiCollision);
									}
									else if (blocks[i]->isBlockUnbreakable())
									{
										ball[k]->collisionBlock(blocks[i]->getRec(), multiCollision);
									}

									multiCollision = false;
									blocksCollisionated = 0;
								}
							}
						}
					}
				}

				if (powerup != NULL)
				{
					if (!powerup->getActived())
					{
						powerup->move();

						if (powerup->checkCollisionPlayer(player->getRec()))
						{
							powerup->setActived(true);
							powerup->start();
						}
					}
					else
					{
						powerup::loop();
					}
				}

				//RestartBall
				for (int i = 0; i < ballLenght; i++)
				{
					if (ball[i] != NULL)
					{
						if (ball[i]->getPosition().y > game::screenHeight)
						{
							delete ball[i];
							ball[i] = NULL;
						}
					}
				}
				if (powerup != NULL)
				{
					if (powerup->getPosition().y > game::screenHeight)
					{
						powerup::deInit();
					}
				}

				if (lostRound())
				{
					player->setLife(player->getLife() - 1);
					powerup::deInit();

					startRound();
				}
			}
			else
			{
				//ShootBall
				if (input::checkMoveRightLeftPlayer())
				{
					if (ball[0] != NULL)
					{
						Vector2 aux = { (player->getRec().x + (player->getRec().width / 2)), (player->getRec().y - ball[0]->getRadius()) };
						ball[0]->setPosition(aux);
						ball[0]->changeAngle();
					}

					setEndPositionLine(lineLarge);
					if (endPositionLine.x <= 0 || endPositionLine.x >= game::screenWidth)
					{
						setSecPositionLine();
					}
				}

				if (input::checkShootBall())
				{
					startGame = true;
				}
			}

			for (int i = 0; i < blockLength; i++)
			{
				if (blocks[i]->getExplosion())
				{
					blocks[i]->explosionAnimation();
				}
			}

			if (player->getLife() <= 0)
			{
				lost_level::init();
				game::gameStatus = game::GAME_STATUS::LOST_LEVEL;
			}
			if (input::checkPauseGame())
			{
				pause::init();
				game::gameStatus = game::GAME_STATUS::PAUSE;
			}
			if (finihLevel())
			{
				powerup::deInit();

				if (levels::isFinishGame())
				{
					finish_game::init();
					game::gameStatus = game::GAME_STATUS::FINISH_GAME;
				}
				else
				{
					won_level::init();
					game::gameStatus = game::GAME_STATUS::WON_LEVEL;
				}
			}
		}

		void draw()
		{
			player->show();

			for (int i = 0; i < ballLenght; i++)
			{
				if (ball[i] != NULL)
				{
					ball[i]->show();
				}
			}

			for (int i = 0; i < blockLength; i++)
			{
				if (!blocks[i]->isBlockEmpty())
				{
					blocks[i]->show();
				}
				else if (blocks[i]->getExplosion())
				{
					blocks[i]->showExplosionAnimation();
				}
			}

			if (powerup != NULL)
			{
				if (!powerup->getActived())
				{
					powerup->show();
				}
				else
				{
					powerup::showPowerupText();
				}
			}

			showHUD();

			if (!startGame)
			{
				DrawLine(static_cast<int>(ball[0]->getPosition().x), static_cast<int>(ball[0]->getPosition().y), static_cast<int>(endPositionLine.x), static_cast<int>(endPositionLine.y), WHITE);

				if (endPositionLine.x <= 0 || endPositionLine.x >= game::screenWidth)
				{
					DrawLine(static_cast<int>(endPositionLine.x), static_cast<int>(endPositionLine.y), static_cast<int>(secPositionLine.x), static_cast<int>(secPositionLine.y), WHITE);
				}
			}
		}

		void deInit()
		{
			if (player != NULL)
			{
				delete player;
			}
			
			ball::deInit();

			for (int i = 0; i < blockLength; i++)
			{
				if (blocks[i] != NULL)
				{
					delete blocks[i];
				}
			}
			powerup::deInit();

			StopMusicStream(audio::gameplayMusic);
		}
	}
}