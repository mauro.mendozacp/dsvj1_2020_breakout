#include "levels.h"
#include "gameplay.h"
#include "game/game.h"

namespace breakout
{
	namespace levels
	{
		const int startLevel = 1;
		const int endLevel = 10;

		static void level1()
		{
			/*
			10 x 12
			------------
			------------
			---o----o---
			---o----o---
			---o----o---
			---oooooo---
			------------
			------------
			------------
			------------
			*/

			gameplay::blocks[27]->setBlockForColor(BLUE);
			gameplay::blocks[32]->setBlockForColor(BLUE);

			gameplay::blocks[39]->setBlockForColor(BLUE);
			gameplay::blocks[44]->setBlockForColor(BLUE);

			gameplay::blocks[51]->setBlockForColor(BLUE);
			gameplay::blocks[56]->setBlockForColor(BLUE);

			for (int i = 63; i <= 68; i++)
			{
				gameplay::blocks[i]->setBlockForColor(BLUE);
			}
		}

		static void level2()
		{
			/*
			10 x 12
			------------
			------------
			---o----o---
			---o----o---
			---oooooo---
			---o----o---
			---o----o---
			------------
			------------
			------------
			*/

			gameplay::blocks[27]->setBlockForColor(YELLOW);
			gameplay::blocks[32]->setBlockForColor(YELLOW);

			gameplay::blocks[39]->setBlockForColor(BLUE);
			gameplay::blocks[44]->setBlockForColor(BLUE);

			for (int i = 51; i <= 56; i++)
			{
				gameplay::blocks[i]->setBlockForColor(YELLOW);
			}

			gameplay::blocks[63]->setBlockForColor(BLUE);
			gameplay::blocks[68]->setBlockForColor(BLUE);

			gameplay::blocks[75]->setBlockForColor(YELLOW);
			gameplay::blocks[80]->setBlockForColor(YELLOW);
		}

		static void level3()
		{
			/*
			10 x 12
			------------
			------------
			------------
			-----oo-----
			-----oo-----
			---oooooo---
			---oooooo---
			------------
			------------
			------------
			*/

			gameplay::blocks[41]->setBlockForColor(BLUE);
			gameplay::blocks[42]->setBlockForColor(BLUE);

			gameplay::blocks[53]->setBlockForColor(BLUE);
			gameplay::blocks[54]->setBlockForColor(BLUE);

			gameplay::blocks[63]->setBlockForColor(YELLOW);
			gameplay::blocks[64]->setBlockForColor(BLUE);
			gameplay::blocks[65]->setBlockForColor(YELLOW);
			gameplay::blocks[66]->setBlockForColor(YELLOW);
			gameplay::blocks[67]->setBlockForColor(BLUE);
			gameplay::blocks[68]->setBlockForColor(YELLOW);

			for (int i = 75; i <= 80; i++)
			{
				gameplay::blocks[i]->setBlockForColor(LIGHTGRAY);
			}
		}

		static void level4()
		{
			/*
			10 x 12
			------------
			------------
			------------
			-----oo-----
			----o--o----
			---o-oo-o---
			--o-o--o-o--
			------------
			------------
			------------
			*/

			gameplay::blocks[41]->setBlockForColor(RED);
			gameplay::blocks[42]->setBlockForColor(RED);

			gameplay::blocks[52]->setBlockForColor(RED);
			gameplay::blocks[55]->setBlockForColor(RED);

			gameplay::blocks[63]->setBlockForColor(YELLOW);
			gameplay::blocks[65]->setBlockForColor(YELLOW);
			gameplay::blocks[66]->setBlockForColor(YELLOW);
			gameplay::blocks[68]->setBlockForColor(YELLOW);

			gameplay::blocks[74]->setBlockForColor(BLUE);
			gameplay::blocks[76]->setBlockForColor(BLUE);
			gameplay::blocks[79]->setBlockForColor(BLUE);
			gameplay::blocks[81]->setBlockForColor(BLUE);
		}

		static void level5()
		{
			/*
			10 x 12
			------------
			------------
			----o--o----
			---o-oo-o---
			--o-o--o-o--
			--o-o--o-o--
			--oooooooo--
			------------
			------------
			------------
			*/

			gameplay::blocks[28]->setBlockForColor(RED);
			gameplay::blocks[31]->setBlockForColor(RED);

			gameplay::blocks[39]->setBlockForColor(BLUE);
			gameplay::blocks[41]->setBlockForColor(BLUE);
			gameplay::blocks[42]->setBlockForColor(BLUE);
			gameplay::blocks[44]->setBlockForColor(BLUE);

			gameplay::blocks[50]->setBlockForColor(BLUE);
			gameplay::blocks[52]->setBlockForColor(YELLOW); gameplay::blocks[52]->setIsBomb(true);
			gameplay::blocks[55]->setBlockForColor(YELLOW); gameplay::blocks[55]->setIsBomb(true);
			gameplay::blocks[57]->setBlockForColor(BLUE);

			gameplay::blocks[62]->setBlockForColor(BLUE);
			gameplay::blocks[64]->setBlockForColor(RED);
			gameplay::blocks[67]->setBlockForColor(RED);
			gameplay::blocks[69]->setBlockForColor(BLUE);

			gameplay::blocks[74]->setBlockForColor(BLUE);
			gameplay::blocks[75]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[76]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[77]->setBlockForColor(BLUE);
			gameplay::blocks[78]->setBlockForColor(BLUE);
			gameplay::blocks[79]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[80]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[81]->setBlockForColor(BLUE);
		}

		static void level6()
		{
			/*
			10 x 12
			------------
			------------
			--oooooooo--
			--o-o--o-o--
			---o-oo-o---
			--o-o--o-o--
			---o-oo-o---
			--o-o--o-o--
			------------
			------------
			*/

			gameplay::blocks[26]->setBlockForColor(BLUE);
			gameplay::blocks[27]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[28]->setBlockForColor(BLUE);
			gameplay::blocks[29]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[30]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[31]->setBlockForColor(BLUE);
			gameplay::blocks[32]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[33]->setBlockForColor(BLUE);

			gameplay::blocks[38]->setBlockForColor(BLUE);
			gameplay::blocks[40]->setBlockForColor(YELLOW);
			gameplay::blocks[43]->setBlockForColor(YELLOW);
			gameplay::blocks[45]->setBlockForColor(BLUE);

			gameplay::blocks[51]->setBlockForColor(YELLOW); gameplay::blocks[51]->setIsBomb(true);
			gameplay::blocks[53]->setBlockForColor(RED);
			gameplay::blocks[54]->setBlockForColor(RED);
			gameplay::blocks[56]->setBlockForColor(YELLOW); gameplay::blocks[56]->setIsBomb(true);

			gameplay::blocks[62]->setBlockForColor(BLUE);
			gameplay::blocks[64]->setBlockForColor(YELLOW);
			gameplay::blocks[67]->setBlockForColor(YELLOW);
			gameplay::blocks[69]->setBlockForColor(BLUE);

			gameplay::blocks[75]->setBlockForColor(YELLOW);
			gameplay::blocks[77]->setBlockForColor(RED);
			gameplay::blocks[78]->setBlockForColor(RED);
			gameplay::blocks[80]->setBlockForColor(YELLOW);

			gameplay::blocks[86]->setBlockForColor(BLUE);
			gameplay::blocks[88]->setBlockForColor(BLUE);
			gameplay::blocks[91]->setBlockForColor(BLUE);
			gameplay::blocks[93]->setBlockForColor(BLUE);
		}

		static void level7()
		{
			/*
			10 x 12
			------------
			------------
			-----oo-----
			--o-oooo-o--
			---o-oo-o---
			--oooooooo--
			-o-o-oo-o-o-
			-oooooooooo-
			------------
			------------
			*/

			gameplay::blocks[29]->setBlockForColor(GREEN);
			gameplay::blocks[30]->setBlockForColor(GREEN);

			gameplay::blocks[38]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[40]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[41]->setBlockForColor(RED);
			gameplay::blocks[42]->setBlockForColor(RED);
			gameplay::blocks[43]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[45]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[51]->setBlockForColor(RED);
			gameplay::blocks[53]->setBlockForColor(RED);
			gameplay::blocks[54]->setBlockForColor(RED);
			gameplay::blocks[56]->setBlockForColor(RED);

			gameplay::blocks[62]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[63]->setBlockForColor(RED);
			gameplay::blocks[64]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[65]->setBlockForColor(YELLOW);
			gameplay::blocks[66]->setBlockForColor(YELLOW);
			gameplay::blocks[67]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[68]->setBlockForColor(RED);
			gameplay::blocks[69]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[73]->setBlockForColor(YELLOW);
			gameplay::blocks[75]->setBlockForColor(YELLOW); gameplay::blocks[75]->setIsBomb(true);
			gameplay::blocks[77]->setBlockForColor(YELLOW);
			gameplay::blocks[78]->setBlockForColor(YELLOW);
			gameplay::blocks[80]->setBlockForColor(YELLOW); gameplay::blocks[80]->setIsBomb(true);
			gameplay::blocks[82]->setBlockForColor(YELLOW);

			gameplay::blocks[85]->setBlockForColor(BLUE);
			gameplay::blocks[86]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[87]->setBlockForColor(BLUE);
			gameplay::blocks[88]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[89]->setBlockForColor(BLUE);
			gameplay::blocks[90]->setBlockForColor(BLUE);
			gameplay::blocks[91]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[92]->setBlockForColor(BLUE);
			gameplay::blocks[93]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[94]->setBlockForColor(BLUE);
		}

		static void level8()
		{
			/*
			10 x 12
			------------
			oooooooooooo
			o----------o
			o---oooo---o
			o--oooooo--o
			o-oooooooo-o
			o----------o
			o----------o
			oooooooooooo
			------------
			*/

			//Arriba
			gameplay::blocks[12]->setBlockForColor(YELLOW);
			for (int i = 13; i <= 22; i++)
			{
				gameplay::blocks[i]->setBlockForColor(RED);
			}
			gameplay::blocks[16]->setIsBomb(true);
			gameplay::blocks[19]->setIsBomb(true);

			gameplay::blocks[23]->setBlockForColor(YELLOW);

			//Paredes
			gameplay::blocks[24]->setBlockForColor(YELLOW);
			gameplay::blocks[35]->setBlockForColor(YELLOW);

			gameplay::blocks[36]->setBlockForColor(YELLOW);
			gameplay::blocks[47]->setBlockForColor(YELLOW);

			gameplay::blocks[48]->setBlockForColor(YELLOW); gameplay::blocks[48]->setIsBomb(true);
			gameplay::blocks[59]->setBlockForColor(YELLOW); gameplay::blocks[59]->setIsBomb(true);

			gameplay::blocks[60]->setBlockForColor(YELLOW);
			gameplay::blocks[71]->setBlockForColor(YELLOW);

			gameplay::blocks[72]->setBlockForColor(YELLOW); gameplay::blocks[72]->setIsBomb(true);
			gameplay::blocks[83]->setBlockForColor(YELLOW); gameplay::blocks[83]->setIsBomb(true);

			gameplay::blocks[84]->setBlockForColor(YELLOW);
			gameplay::blocks[95]->setBlockForColor(YELLOW);

			//Centro
			for (int i = 40; i <= 43; i++)
			{
				gameplay::blocks[i]->setBlockForColor(BLUE);
			}

			gameplay::blocks[51]->setBlockForColor(BLUE);
			gameplay::blocks[52]->setBlockForColor(BLUE);
			gameplay::blocks[53]->setBlockForColor(GREEN); gameplay::blocks[53]->setIsBomb(true);
			gameplay::blocks[54]->setBlockForColor(GREEN); gameplay::blocks[54]->setIsBomb(true);
			gameplay::blocks[55]->setBlockForColor(BLUE);
			gameplay::blocks[56]->setBlockForColor(BLUE);

			gameplay::blocks[62]->setBlockForColor(GREEN);
			gameplay::blocks[63]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[64]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[65]->setBlockForColor(GREEN);
			gameplay::blocks[66]->setBlockForColor(GREEN);
			gameplay::blocks[67]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[68]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[69]->setBlockForColor(GREEN);

			//Abajo
			for (int i = 96; i <= 107; i++)
			{
				gameplay::blocks[i]->setBlockForColor(BLUE);
			}
		}

		static void level9()
		{
			/*
			10 x 12
			--o-oooo-o--
			---o-oo-o---
			--ooo--ooo--
			--o-o--o-o--
			--o------o--
			--o--o---o--
			--o-o--o-o--
			--o--oo--o--
			---o----o---
			--o-oooo-o--
			*/

			gameplay::blocks[2]->setBlockForColor(LIGHTGRAY);
			for (int i = 4; i <= 7; i++)
			{
				gameplay::blocks[i]->setBlockForColor(YELLOW);
			}
			gameplay::blocks[9]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[15]->setBlockForColor(YELLOW);
			gameplay::blocks[17]->setBlockForColor(BLUE);
			gameplay::blocks[18]->setBlockForColor(BLUE);
			gameplay::blocks[20]->setBlockForColor(YELLOW);

			gameplay::blocks[26]->setBlockForColor(YELLOW);
			gameplay::blocks[27]->setBlockForColor(BLUE);
			gameplay::blocks[28]->setBlockForColor(BLUE);
			gameplay::blocks[31]->setBlockForColor(BLUE);
			gameplay::blocks[32]->setBlockForColor(BLUE);
			gameplay::blocks[33]->setBlockForColor(YELLOW);

			gameplay::blocks[38]->setBlockForColor(YELLOW);
			gameplay::blocks[40]->setBlockForColor(SKYBLUE); gameplay::blocks[40]->setIsBomb(true);
			gameplay::blocks[43]->setBlockForColor(SKYBLUE); gameplay::blocks[43]->setIsBomb(true);
			gameplay::blocks[45]->setBlockForColor(YELLOW);

			gameplay::blocks[50]->setBlockForColor(YELLOW);
			gameplay::blocks[57]->setBlockForColor(YELLOW);

			gameplay::blocks[62]->setBlockForColor(YELLOW);
			gameplay::blocks[65]->setBlockForColor(RED); gameplay::blocks[65]->setIsBomb(true);
			gameplay::blocks[69]->setBlockForColor(YELLOW);

			gameplay::blocks[74]->setBlockForColor(YELLOW);
			gameplay::blocks[76]->setBlockForColor(GREEN);
			gameplay::blocks[79]->setBlockForColor(GREEN);
			gameplay::blocks[81]->setBlockForColor(YELLOW);

			gameplay::blocks[86]->setBlockForColor(YELLOW);
			gameplay::blocks[89]->setBlockForColor(GREEN);
			gameplay::blocks[90]->setBlockForColor(GREEN);
			gameplay::blocks[93]->setBlockForColor(YELLOW);

			gameplay::blocks[99]->setBlockForColor(YELLOW);
			gameplay::blocks[104]->setBlockForColor(YELLOW);

			gameplay::blocks[110]->setBlockForColor(LIGHTGRAY);
			for (int i = 112; i <= 115; i++)
			{
				gameplay::blocks[i]->setBlockForColor(YELLOW);
			}
			gameplay::blocks[117]->setBlockForColor(LIGHTGRAY);
		}

		static void level10()
		{
			/*
			10 x 12
			------------
			--o------o--
			ooo--oo--ooo
			---oooooo---
			o---oooo---o
			oooo-oo-oooo
			oooo-oo-oooo
			ooo--oo--ooo
			ooooo--ooooo
			------------
			*/

			gameplay::blocks[14]->setBlockForColor(BLUE);
			gameplay::blocks[21]->setBlockForColor(BLUE);

			gameplay::blocks[24]->setBlockForColor(BLUE);
			gameplay::blocks[25]->setBlockForColor(SKYBLUE);
			gameplay::blocks[26]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[29]->setBlockForColor(SKYBLUE);
			gameplay::blocks[30]->setBlockForColor(SKYBLUE);
			gameplay::blocks[33]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[34]->setBlockForColor(SKYBLUE);
			gameplay::blocks[35]->setBlockForColor(BLUE);

			gameplay::blocks[39]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[40]->setBlockForColor(YELLOW); gameplay::blocks[40]->setIsBomb(true);
			gameplay::blocks[41]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[42]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[43]->setBlockForColor(YELLOW); gameplay::blocks[43]->setIsBomb(true);
			gameplay::blocks[44]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[52]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[53]->setBlockForColor(BLUE); gameplay::blocks[53]->setIsBomb(true);
			gameplay::blocks[54]->setBlockForColor(BLUE); gameplay::blocks[54]->setIsBomb(true);
			gameplay::blocks[55]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[60]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[61]->setBlockForColor(GREEN);
			gameplay::blocks[63]->setBlockForColor(GREEN);
			gameplay::blocks[65]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[66]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[68]->setBlockForColor(GREEN);
			gameplay::blocks[70]->setBlockForColor(GREEN);
			gameplay::blocks[71]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[72]->setBlockForColor(BLUE);
			gameplay::blocks[73]->setBlockForColor(BLUE);
			gameplay::blocks[74]->setBlockForColor(RED); gameplay::blocks[74]->setIsBomb(true);
			gameplay::blocks[77]->setBlockForColor(RED);
			gameplay::blocks[78]->setBlockForColor(RED);
			gameplay::blocks[81]->setBlockForColor(RED); gameplay::blocks[81]->setIsBomb(true);
			gameplay::blocks[82]->setBlockForColor(BLUE);
			gameplay::blocks[83]->setBlockForColor(BLUE);

			gameplay::blocks[84]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[85]->setBlockForColor(YELLOW);
			gameplay::blocks[86]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[89]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[90]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[93]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[94]->setBlockForColor(YELLOW);
			gameplay::blocks[95]->setBlockForColor(LIGHTGRAY);

			gameplay::blocks[96]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[97]->setBlockForColor(YELLOW);
			gameplay::blocks[98]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[99]->setBlockForColor(RED);
			gameplay::blocks[100]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[103]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[104]->setBlockForColor(RED);
			gameplay::blocks[105]->setBlockForColor(LIGHTGRAY);
			gameplay::blocks[106]->setBlockForColor(YELLOW);
			gameplay::blocks[107]->setBlockForColor(LIGHTGRAY);
		}

		static void setLevel()
		{
			switch (gameplay::levelGame)
			{
			case 1: level1();
				break;
			case 2: level2();
				break;
			case 3: level3();
				break;
			case 4: level4();
				break;
			case 5: level5();
				break;
			case 6: level6();
				break;
			case 7: level7();
				break;
			case 8: level8();
				break;
			case 9: level9();
				break;
			case 10: level10();
				break;
			default:
				break;
			}
		}

		bool isFinishGame()
		{
			if ((gameplay::levelGame + 1) > endLevel)
			{
				return true;
			}

			return false;
		}

		void startLevelGame()
		{
			gameplay::levelGame = startLevel;
			setLevel();
		}

		void nextLevel()
		{
			if (!isFinishGame())
			{
				gameplay::levelGame++;
				block::clear();
				setLevel();
			}
		}
	}
}