#pragma once
#ifndef LEVELS_H
#define LEVELS_H

#include "raylib.h"

namespace breakout
{
	namespace levels
	{
		bool isFinishGame();
		void startLevelGame();
		void nextLevel();
	}
}

#endif // !LEVELS_H