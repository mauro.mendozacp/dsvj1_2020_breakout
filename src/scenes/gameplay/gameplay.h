#pragma once
#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include <math.h>

#include "raylib.h"

#include "entities/player/player.h"
#include "entities/ball/ball.h"
#include "entities/block/block.h"
#include "entities/powerup/powerup.h"
#include "levels.h"

namespace breakout
{
	namespace gameplay
	{
		extern player::Player* player;
		extern const int ballLenght;
		extern ball::Ball* ball[];
		extern const int blockRow;
		extern const int blockCol;
		extern const int blockLength;
		extern block::Block* blocks[];
		extern powerup::Powerup* powerup;

		extern bool startGame;
		extern int levelGame;

		void init();
		void update();
		void draw();
		void deInit();

		void startRound();
	}
}

#endif // !GAMEPLAY_H