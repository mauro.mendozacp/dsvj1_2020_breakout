#include "game/game.h"

using namespace breakout;

int main(void)
{
    game::runGame();

    return 0;
}