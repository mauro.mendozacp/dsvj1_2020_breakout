#include "block.h"
#include "game/game.h"

namespace breakout
{
	namespace block
	{
		const int colorLenght = 6;
		int points_a[colorLenght] = { 0, 10, 15, 25, 50, 100 };
		int lifes_a[colorLenght] = { 0, 1, 2, 3, 4, 5 };
		Color colors_a[colorLenght] = { LIGHTGRAY, BLUE, YELLOW, RED, GREEN, SKYBLUE };

		float blockPaddleHeight = 30;
		float blockPaddleWidth = static_cast<float>((game::screenWidth / gameplay::blockCol));

		void init()
		{
			Rectangle auxBlock;
			auxBlock.width = blockPaddleWidth;
			auxBlock.height = blockPaddleHeight;

			float startPosX = 0;
			float startPosY = static_cast<float>((game::screenHeight) / 4);
			for (int i = 0; i < gameplay::blockRow; i++)
			{
				for (int j = 0; j < gameplay::blockCol; j++)
				{
					auxBlock.x = startPosX + auxBlock.width * j;
					auxBlock.y = startPosY + auxBlock.height * i;
					gameplay::blocks[j + (gameplay::blockCol * i)] = new block::Block(auxBlock);
				}
			}
		}

		void clear()
		{
			
			for (int i = 0; i < gameplay::blockLength; i++)
			{
				gameplay::blocks[i]->setTexture(Texture2D());
				gameplay::blocks[i]->setColor(Color());
				gameplay::blocks[i]->setIsBomb(false);
				gameplay::blocks[i]->setLife(0);
				gameplay::blocks[i]->setPoints(0);
				gameplay::blocks[i]->setExplosion(false);
				gameplay::blocks[i]->setAnimation(Animation());
			}
		}

		static bool checkBlockDestroy(int index)
		{
			if (index >= 0 && index < gameplay::blockLength)
			{
				if (gameplay::blocks[index]->getBlockType() == BLOCK_TYPE::BREAKABLE)
				{
					return true;
				}
			}

			return false;
		}

		static void destroyBomb(int indexMid)
		{
			int start = indexMid - gameplay::blockCol - 1;
			int index = 0;

			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (i == 1 && j == 1)
					{
						continue;
					}

					index = start + (gameplay::blockCol * i) + j;

					if (checkBlockDestroy(index))
					{
						if (j == 0)
						{
							if ((index % gameplay::blockCol) > (indexMid % gameplay::blockCol))
							{
								continue;
							}
						}
						if (j == 2)
						{
							if ((index % gameplay::blockCol) < (indexMid % gameplay::blockCol))
							{
								continue;
							}
						}

						destroyBlock(index);
					}
				}
			}
		}

		void destroyBlock(int index)
		{
			gameplay::blocks[index]->setBlockType(block::BLOCK_TYPE::EMPTY);
			gameplay::player->setScore(gameplay::player->getScore() + gameplay::blocks[index]->getPoints());

			if (gameplay::powerup == NULL)
			{
				if (powerup::checkChance())
				{
					powerup::init(gameplay::blocks[index]->getRec());
				}
			}

			if (gameplay::blocks[index]->getIsBomb())
			{
				PlaySound(audio::explosionSound);
				gameplay::blocks[index]->setExplosion(true);
				destroyBomb(index);
			}
		}

		int getLifeForColor(Color color)
		{
			for (int i = 0; i < colorLenght; i++)
			{
				if (ColorToInt(colors_a[i]) == ColorToInt(color))
				{
					return lifes_a[i];
				}
			}

			return 0;
		}

		int getPointsForColor(Color color)
		{
			for (int i = 0; i < colorLenght; i++)
			{
				if (ColorToInt(colors_a[i]) == ColorToInt(color))
				{
					return points_a[i];
				}
			}

			return 0;
		}

		BLOCK_TYPE getBlockTypeForColor(Color color)
		{
			if (ColorToInt(colors_a[0]) == ColorToInt(color))
			{
				return BLOCK_TYPE::UNBREAKABLE;
			}

			for (int i = 1; i < colorLenght; i++)
			{
				if (ColorToInt(colors_a[i]) == ColorToInt(color))
				{
					return BLOCK_TYPE::BREAKABLE;
				}
			}

			return BLOCK_TYPE();
		}

		Texture2D getTexture2DForColor(Color color)
		{
			Texture2D text = Texture2D();

			if (ColorToInt(color) == ColorToInt(colors_a[0]))
			{
				return texture::unbreakableBlock;
			}
			if (ColorToInt(color) == ColorToInt(colors_a[1]))
			{
				return texture::blueBlock;
			}
			if (ColorToInt(color) == ColorToInt(colors_a[2]))
			{
				return texture::yellowBlock;
			}
			if (ColorToInt(color) == ColorToInt(colors_a[3]))
			{
				return texture::redBlock;
			}
			if (ColorToInt(color) == ColorToInt(colors_a[4]))
			{
				return texture::greenBlock;
			}
			if (ColorToInt(color) == ColorToInt(colors_a[5]))
			{
				return texture::skyblueBlock;
			}

			return text;
		}

		Block::Block()
		{
			this->rec = Rectangle();
			this->color = Color();
			this->texture = Texture2D();
			this->life = 0;
			this->points = 0;
			this->isBomb = false;
			this->explosion = false;
			this->blockType = BLOCK_TYPE::EMPTY;
			this->animation = Animation();
		}

		Block::Block(Rectangle rec)
		{
			this->rec = rec;
			this->color = Color();
			this->texture = Texture2D();
			this->life = 0;
			this->points = 0;
			this->isBomb = false;
			this->explosion = false;
			this->blockType = BLOCK_TYPE::EMPTY;
			this->animation = Animation();
		}

		Block::~Block()
		{
		}

		void Block::show()
		{
			DrawTextureRec(texture, { 0, 0, blockPaddleWidth, blockPaddleHeight }, getPosition(), color);
			
			#if DEBUG
				DrawRectangleLines(static_cast<int>(rec.x), static_cast<int>(rec.y), static_cast<int>(rec.width), static_cast<int>(rec.height), GREEN);
			#endif // DEBUG

			if (isBomb)
			{
				Vector2 pos = getPosition();
				pos.x += (blockPaddleWidth) / 2 - (static_cast<float>(texture::bomb.width) / 2);
				pos.y += (blockPaddleHeight) / 2 - (static_cast<float>(texture::bomb.height) / 2);
				DrawTextureRec(texture::bomb, { 0, 0, static_cast<float>(texture::bomb.width), static_cast<float>(texture::bomb.height) }, pos, WHITE);
			}
		}

		void Block::showExplosionAnimation()
		{
			Vector2 pos = getPosition();
			pos.x += (blockPaddleWidth) / 2 - ((static_cast<float>(texture::explosion.width) / animation.animations) / 2);
			pos.y += (blockPaddleHeight) / 2 - (static_cast<float>(texture::explosion.height) / 2);
			DrawTextureRec(texture::explosion, animation.frameRec, pos, WHITE);
		}

		void Block::explosionAnimation()
		{
			animation.framesCounter++;

			if (animation.framesCounter >= (game::frames / animation.framesSpeed))
			{
				animation.framesCounter = 0;
				animation.currentFrame++;

				if (animation.currentFrame > (animation.animations - 1))
				{
					animation.currentFrame = 0;
					explosion = false;
				}

				animation.frameRec.x = static_cast<float>(animation.currentFrame) * (static_cast<float>(texture::explosion.width) / animation.animations);
			}
		}

		void Block::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Block::getRec()
		{
			return this->rec;
		}

		void Block::setColor(Color color)
		{
			this->color = color;
		}

		Color Block::getColor()
		{
			return this->color;
		}

		void Block::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Texture2D Block::getTexture()
		{
			return this->texture;
		}

		void Block::setLife(int life)
		{
			this->life = life;
		}

		int Block::getLife()
		{
			return this->life;
		}

		void Block::setPoints(int points)
		{
			this->points = points;
		}

		int Block::getPoints()
		{
			return this->points;
		}

		void Block::setIsBomb(bool isBomb)
		{
			this->isBomb = isBomb;
		}

		bool Block::getIsBomb()
		{
			return this->isBomb;
		}

		void Block::setExplosion(bool explosion)
		{
			this->explosion = explosion;
		}

		bool Block::getExplosion()
		{
			return this->explosion;
		}

		void Block::setBlockType(BLOCK_TYPE blockType)
		{
			this->blockType = blockType;
		}

		BLOCK_TYPE Block::getBlockType()
		{
			return this->blockType;
		}

		void Block::setAnimation(Animation animation)
		{
			this->animation = animation;
		}

		Animation Block::getAnimation()
		{
			return this->animation;
		}

		void Block::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		Vector2 Block::getPosition()
		{
			return Vector2({ this->rec.x, this->rec.y });
		}

		bool Block::isBlockEmpty()
		{
			if (blockType == BLOCK_TYPE::EMPTY)
			{
				return true;
			}

			return false;
		}

		bool Block::isBlockUnbreakable()
		{
			if (blockType == BLOCK_TYPE::UNBREAKABLE)
			{
				return true;
			}

			return false;
		}

		void Block::setBlockForColor(Color color)
		{
			this->color = color;
			this->life = getLifeForColor(color);
			this->points = getPointsForColor(color);
			this->blockType = getBlockTypeForColor(color);
			this->texture = getTexture2DForColor(color);
		}
	}
}