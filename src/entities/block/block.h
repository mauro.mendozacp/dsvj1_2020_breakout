#pragma once
#ifndef BLOCK_H
#define BLOCK_H

#include "raylib.h"
#include "texture/texture.h"

namespace breakout
{
	namespace block
	{
		enum class BLOCK_TYPE
		{
			BREAKABLE = 1,
			UNBREAKABLE,
			EMPTY
		};

		struct Animation
		{
			Rectangle frameRec;
			int currentFrame;
			float framesCounter;
			float framesSpeed;
			int animations;

			Animation()
			{
				currentFrame = 0;
				framesCounter = 0;
				framesSpeed = 8.0f;
				animations = 8;
				frameRec = { 0.0f, 0.0f, static_cast<float>(texture::explosion.width) / animations, static_cast<float>(texture::explosion.height) };
			}
		};

		extern float blockPaddleHeight;
		extern float blockPaddleWidth;

		void init();
		void clear();
		void destroyBlock(int index);

		class Block
		{
		public:
			Block();
			Block(Rectangle rec);
			~Block();

			void show();
			void showExplosionAnimation();
			void explosionAnimation();

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setColor(Color color);
			Color getColor();
			void setTexture(Texture2D texture);
			Texture2D getTexture();
			void setLife(int life);
			int getLife();
			void setPoints(int points);
			int getPoints();
			void setIsBomb(bool isBomb);
			bool getIsBomb();
			void setExplosion(bool explosion);
			bool getExplosion();
			void setBlockType(BLOCK_TYPE blockType);
			BLOCK_TYPE getBlockType();
			void setAnimation(Animation animation);
			Animation getAnimation();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			bool isBlockEmpty();
			bool isBlockUnbreakable();
			void setBlockForColor(Color color);
		private:
			Rectangle rec;
			Color color;
			Texture2D texture;
			int life;
			int points;
			bool isBomb;
			bool explosion;
			BLOCK_TYPE blockType;
			Animation animation;
		};
	}
}

#endif // !BLOCK_H
