#include "powerup.h"
#include "game/game.h"

namespace breakout
{
	namespace powerup
	{
		const int powerupLenght = 4;
		const char* powerupText[powerupLenght] = { "MULTIBALL", "FIREBALL", "PADDLE MODIFIER", "BALL MODIFIER" };

		const float percentIncreasePaddle = 125;
		const float percentDecreasePaddle = 75;
		const float percentDecreaseBallSpeed = 50;

		float timer = 0;
		float timePowerup = 15.0f;
		float timerText = 3.0f;

		void init(Rectangle block)
		{
			timer = 0;
			float rad = 8.0f;
			float velX = 150.0f;
			Color col = WHITE;

			Vector2 pos;
			pos.x = block.x + (block.width / 2);
			pos.y = block.y + (block.height / 2);

			POWERUP_TYPE powt = static_cast<POWERUP_TYPE>(GetRandomValue(1, powerupLenght));

			gameplay::powerup = new Powerup(pos, rad, velX, powt, col, false);
		}

		void loop()
		{
			timer += GetFrameTime();
			gameplay::powerup->loop();

			if (gameplay::powerup->getPowerupType() == POWERUP_TYPE::FIREBALL && timer >= timePowerup)
			{
				deInit();
			}
		}

		void showPowerupText()
		{
			if (timer < timerText)
			{
				const char* text = gameplay::powerup->getPowerupText();
				int posX = game::screenWidth / 2;
				int posY = game::screenHeight * 3 / 4;
				int font = 30;
				Color color = LIGHTGRAY;

				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);
			}
		}

		bool checkChance()
		{
			if (GetRandomValue(1, 100) < gameplay_settings::chancePowerup)
			{
				return true;
			}

			return false;
		}

		void deInit()
		{
			if (gameplay::powerup != NULL)
			{
				gameplay::powerup->finish();

				delete gameplay::powerup;
				gameplay::powerup = NULL;
			}
		}

		Powerup::Powerup()
		{
			this->position = Vector2();
			this->radius = 0;
			this->velocityY = 0;
			this->powerupType = POWERUP_TYPE();
			this->color = Color();
			this->actived = false;
		}

		Powerup::Powerup(Vector2 position, float radius, float velocityY, POWERUP_TYPE powerupType, Color color, bool actived)
		{
			this->position = position;
			this->radius = radius;
			this->velocityY = velocityY;
			this->powerupType = powerupType;
			this->color = color;
			this->actived = actived;
		}

		Powerup::~Powerup()
		{
		}

		void Powerup::move()
		{
			position.y += velocityY * GetFrameTime();
		}

		void Powerup::show()
		{
			DrawCircle(static_cast<int>(position.x), static_cast<int>(position.y), radius, color);

			#if DEBUG
				DrawCircleLines(static_cast<int>(position.x), static_cast<int>(position.y), radius, GREEN);
			#endif // DEBUG
		}

		bool Powerup::checkCollisionPlayer(Rectangle player)
		{
			if (CheckCollisionCircleRec(position, radius, player))
			{
				return true;
			}

			return false;
		}

		void Powerup::start()
		{
			switch (powerupType)
			{
			case POWERUP_TYPE::MULTIBALL:
				ball::start(1, gameplay::ball[0]->getPosition(), gameplay::ball[0]->getNewVelocityForAngle(gameplay::ball[0]->getAngle() + 15), gameplay::ball[0]->getAngle() + 15);
				ball::start(2, gameplay::ball[0]->getPosition(), gameplay::ball[0]->getNewVelocityForAngle(gameplay::ball[0]->getAngle() - 15), gameplay::ball[0]->getAngle() - 15);
				break;
			case POWERUP_TYPE::FIREBALL:
				if (gameplay::ball[0] != NULL)
				{
					gameplay::ball[0]->setFireball(true);
					gameplay::ball[0]->setTexture(texture::ballRed);
				}
				
				break;
			case POWERUP_TYPE::PADDLE_MODIFIER:
				if (GetRandomValue(0, 1) == 0)
				{
					gameplay::player->setSize((gameplay::player->getWidth() * percentIncreasePaddle / 100), gameplay::player->getHeight(), false);
				}
				else
				{
					gameplay::player->setSize((gameplay::player->getWidth() * percentDecreasePaddle / 100), gameplay::player->getHeight(), true);
				}
				break;
			case POWERUP_TYPE::BALL_MODIFIER:
				ball::ballSpeed = ball::ballSpeed * 50 / 100;
				
				if (gameplay::ball[0] != NULL)
				{
					gameplay::ball[0]->setVelocity(gameplay::ball[0]->getNewVelocityForAngle(gameplay::ball[0]->getAngle()));
				}

				break;
			default:
				break;
			}
		}

		void Powerup::loop()
		{
			switch (powerupType)
			{
			case POWERUP_TYPE::MULTIBALL:
				break;
			case POWERUP_TYPE::FIREBALL:
				break;
			case POWERUP_TYPE::PADDLE_MODIFIER:
				break;
			case POWERUP_TYPE::BALL_MODIFIER:
				break;
			default:
				break;
			}
		}

		void Powerup::finish()
		{
			switch (powerupType)
			{
			case POWERUP_TYPE::MULTIBALL:
				ball::deInit();
				break;
			case POWERUP_TYPE::FIREBALL:
				if (gameplay::ball[0] != NULL)
				{
					gameplay::ball[0]->setFireball(false);
					gameplay::ball[0]->setTexture(texture::ball);
				}

				break;
			case POWERUP_TYPE::PADDLE_MODIFIER:
				if (player::playerPaddleWidth < gameplay::player->getRec().width)
				{
					gameplay::player->setSize(player::playerPaddleWidth, player::playerPaddleHeight, true);
				}
				else
				{
					gameplay::player->setSize(player::playerPaddleWidth, player::playerPaddleHeight, false);
				}

				break;
			case POWERUP_TYPE::BALL_MODIFIER:
				ball::ballSpeed = gameplay_settings::ballSpeed;

				if (gameplay::ball[0] != NULL)
				{
					gameplay::ball[0]->setVelocity(gameplay::ball[0]->getNewVelocityForAngle(gameplay::ball[0]->getAngle()));
				}
				break;
			default:
				break;
			}
		}

		Vector2 Powerup::getPosition()
		{
			return this->position;
		}

		void Powerup::setPosition(Vector2 position)
		{
			this->position = position;
		}

		float Powerup::getRadius()
		{
			return this->radius;
		}

		void Powerup::setRadius(float radius)
		{
			this->radius = radius;
		}

		float Powerup::getVelocityY()
		{
			return this->velocityY;
		}

		void Powerup::setVelocityY(float velocityY)
		{
			this->velocityY = velocityY;
		}

		POWERUP_TYPE Powerup::getPowerupType()
		{
			return this->powerupType;
		}

		void Powerup::setPowerupType(POWERUP_TYPE powerupType)
		{
			this->powerupType = powerupType;
		}

		Color Powerup::getColor()
		{
			return this->color;
		}

		void Powerup::setColor(Color color)
		{
			this->color = color;
		}

		bool Powerup::getActived()
		{
			return this->actived;
		}

		void Powerup::setActived(bool actived)
		{
			this->actived = actived;
		}

		const char* Powerup::getPowerupText()
		{
			if (static_cast<int>(powerupType) >= 1 && static_cast<int>(powerupType) <= 4)
			{
				return powerupText[static_cast<int>(powerupType) - 1];
			}

			return " ";
		}
	}
}