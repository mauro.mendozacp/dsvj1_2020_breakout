#pragma once
#ifndef POWERUP_H
#define POWERUP_H

#include "raylib.h"

namespace breakout
{
	namespace powerup
	{
		enum class POWERUP_TYPE
		{
			MULTIBALL = 1,
			FIREBALL,
			PADDLE_MODIFIER,
			BALL_MODIFIER
		};

		void init(Rectangle block);
		void loop();
		void showPowerupText();
		bool checkChance();
		void deInit();

		class Powerup
		{
		public:
			Powerup();
			Powerup(Vector2 position, float radius, float velocityY, POWERUP_TYPE powerupType, Color color, bool actived);
			~Powerup();

			void move();
			void show();
			bool checkCollisionPlayer(Rectangle player);
			void start();
			void loop();
			void finish();

			Vector2 getPosition();
			void setPosition(Vector2 position);
			float getRadius();
			void setRadius(float radius);
			float getVelocityY();
			void setVelocityY(float velocityY);
			POWERUP_TYPE getPowerupType();
			void setPowerupType(POWERUP_TYPE powerupType);
			Color getColor();
			void setColor(Color color);
			bool getActived();
			void setActived(bool actived);

			const char* getPowerupText();

		private:
			Vector2 position;
			float radius;
			float velocityY;
			POWERUP_TYPE powerupType;
			Color color;
			bool actived;
		};
	}
}

#endif // !POWERUP_H