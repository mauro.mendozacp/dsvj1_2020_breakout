#include "player.h"
#include "game/game.h"

namespace breakout
{
	namespace player
	{
		float playerPaddleHeight = 20;
		float playerPaddleWidth = 150;
		Vector2 playerStartPosition = { ((static_cast<float>(game::screenWidth) / 2) - (playerPaddleWidth / 2)), (static_cast<float>(game::screenHeight) * 7 / 8) };

		void init()
		{
			Color playerColor = LIGHTGRAY;

			Rectangle recPlayer;
			recPlayer.x = playerStartPosition.x;
			recPlayer.y = playerStartPosition.y;
			recPlayer.height = playerPaddleHeight;
			recPlayer.width = playerPaddleWidth;

			gameplay::player = new Player(recPlayer, playerColor, texture::paddle, gameplay_settings::playerStartLifes, gameplay_settings::playerSpeed, 0);
		}
		
		Player::Player()
		{
			this->rec = Rectangle();
			this->color = Color();
			this->texture = Texture2D();
			this->life = 0;
			this->speed = 0;
			this->score = 0;
		}

		Player::Player(Rectangle rec, Color color, Texture2D texture, int life, float speed, int score)
		{
			this->rec = rec;
			this->color = color;
			this->texture = texture;
			this->life = life;
			this->speed = speed;
			this->score = score;
		}

		Player::~Player()
		{
		}

		void Player::move()
		{
			if (input::checkMoveRightLeftPlayer())
			{
				float auxPosX = getNewPositionX();

				if (checkPositionX(auxPosX))
				{
					rec.x = auxPosX;
				}
			}
		}

		void Player::show()
		{
			DrawTextureRec(texture, { 0, 0, rec.width, rec.height }, getPosition(), color);

			#if DEBUG
				DrawRectangleLines(static_cast<int>(rec.x), static_cast<int>(rec.y), static_cast<int>(rec.width), static_cast<int>(rec.height), GREEN);
			#endif // DEBUG
		}

		void Player::reset()
		{
			setPosition(playerStartPosition);
		}

		void Player::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Player::getRec()
		{
			return this->rec;
		}

		void Player::setColor(Color color)
		{
			this->color = color;
		}

		Color Player::getColor()
		{
			return this->color;
		}

		void Player::setTexture(Texture2D texture)
		{
		}

		Texture2D Player::getTexture()
		{
			return Texture2D();
		}

		void Player::setLife(int life)
		{
			this->life = life;
		}

		int Player::getLife()
		{
			return this->life;
		}

		void Player::setSpeed(float speed)
		{
			this->speed = speed;
		}

		float Player::getSpeed()
		{
			return this->speed;
		}

		void Player::setScore(int score)
		{
			this->score = score;
		}

		int Player::getScore()
		{
			return this->score;
		}

		void Player::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		Vector2 Player::getPosition()
		{
			Vector2 position;
			position.x = this->rec.x;
			position.y = this->rec.y;

			return position;
		}

		void Player::setSize(float width, float height, bool decrease)
		{
			if (decrease)
			{
				this->rec.x -= (width - this->rec.width) / 2;
			}
			else
			{
				this->rec.x += (this->rec.width - width) / 2;
			}

			this->rec.width = width;
			this->rec.height = height;
			this->texture.width = static_cast<int>(width);
			this->texture.height = static_cast<int>(height);
		}

		float Player::getWidth()
		{
			return this->rec.width;
		}

		float Player::getHeight()
		{
			return this->rec.height;
		}

		float Player::getNewPositionX()
		{
			if (input::checkMoveRightPlayer())
			{
				return (rec.x + (speed * 1.0f) * GetFrameTime());
			}
			if (input::checkMoveLeftPlayer())
			{
				return (rec.x + (speed * -1.0f) * GetFrameTime());
			}

			return rec.x;
		}

		bool Player::checkPositionX(float auxPosX)
		{
			if (!gameplay::startGame)
			{
				if (auxPosX + (rec.width / 2) - ball::ballRadius > 0 && (auxPosX + (rec.width / 2)) + ball::ballRadius < game::screenWidth)
				{
					return true;
				}
			}
			else
			{
				if (auxPosX + (rec.width / 2) > 0 && (auxPosX + (rec.width / 2)) < game::screenWidth)
				{
					return true;
				}
			}

			return false;
		}
	}
}