#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

namespace breakout
{
	namespace player
	{
		extern float playerPaddleHeight;
		extern float playerPaddleWidth;
		extern Vector2 playerStartPosition;
		
		void init();

		class Player
		{
		public:
			Player();
			Player(Rectangle rec, Color color, Texture2D texture, int life, float speed, int score);
			~Player();

			void move();
			void show();
			void reset();

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setColor(Color color);
			Color getColor();
			void setTexture(Texture2D texture);
			Texture2D getTexture();
			void setLife(int life);
			int getLife();
			void setSpeed(float speed);
			float getSpeed();
			void setScore(int score);
			int getScore();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			void setSize(float width, float height, bool decrease);
			float getWidth();
			float getHeight();

		private:
			Rectangle rec;
			Color color;
			Texture2D texture;
			int life;
			float speed;
			int score;

			float getNewPositionX();
			bool checkPositionX(float auxPosX);
		};
	}
}

#endif // !PLAYER_H
