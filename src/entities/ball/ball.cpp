#include "ball.h"
#include "game/game.h"

namespace breakout
{
	namespace ball
	{
		float ballSpeed;
		float ballRadius = 10.0f;
		float ballStartAngle = 90;
		float ballMinAngle = 20;

		Vector2 ballStartPosition;
		Vector2 ballStartVelocity;

		void init()
		{
			for (int i = 0; i < gameplay::ballLenght; i++)
			{
				gameplay::ball[i] = NULL;
			}

			ballSpeed = gameplay_settings::ballSpeed;
			ballStartVelocity = { 0, -ballSpeed };

			ballStartPosition = { player::playerStartPosition.x + (player::playerPaddleWidth / 2), (player::playerStartPosition.y - ballRadius - 1) };

			gameplay::ball[0] = new Ball(ballStartPosition, ballRadius, WHITE, texture::ball, ballStartVelocity, ballStartAngle, false, false);
		}

		void start(int index, Vector2 pos, Vector2 vel, float angle)
		{
			gameplay::ball[index] = new Ball(pos, ballRadius, WHITE, texture::ball, vel, angle, false, false);
		}

		void reset()
		{
			start(0, ballStartPosition, ballStartVelocity, ballStartAngle);
		}

		void deInit()
		{
			for (int i = 0; i < gameplay::ballLenght; i++)
			{
				if (gameplay::ball[i] != NULL)
				{
					delete gameplay::ball[i];
					gameplay::ball[i] = NULL;
				}
			}
		}

		Vector2 getVelocityForAngle(float angle, float velocityMax)
		{
			Vector2 auxVelocity = { 0, 0 };

			auxVelocity.x = velocityMax * static_cast<float>(cos(static_cast<double>(angle) * PI / 180));
			auxVelocity.y = static_cast<float>(sqrt(pow(static_cast<double>(velocityMax), 2) - pow(static_cast<double>(auxVelocity.x), 2)));

			return auxVelocity;
		}

		Ball::Ball()
		{
			this->position = Vector2();
			this->radius = 0;
			this->color = Color();
			this->texture = Texture2D();
			this->velocity = Vector2();
			this->angle = 0;
			this->collision = false;
			this->fireball = false;
		}

		Ball::Ball(Vector2 position, float radius, Color color, Texture2D texture, Vector2 velocity, float angle, bool collision, bool fireball)
		{
			this->position = position;
			this->radius = radius;
			this->color = color;
			this->texture = texture;
			this->velocity = velocity;
			this->angle = angle;
			this->collision = collision;
			this->fireball = fireball;
		}

		Ball::~Ball()
		{
		}

		void Ball::move()
		{
			position.x += velocity.x * GetFrameTime();
			position.y += velocity.y * GetFrameTime();
		}

		void Ball::reset()
		{
			setPosition({ player::playerStartPosition.x + (player::playerPaddleWidth / 2) , player::playerStartPosition.y - ballRadius });
			setVelocity(ball::ballStartVelocity);
			setAngle(ballStartAngle);
		}

		Vector2 Ball::getNewVelocityForAngle(float angle)
		{
			Vector2 auxVel = getVelocityForAngle(angle, ballSpeed);

			if (velocity.x < 0)
			{
				auxVel.x *= -1;
			}
			if (velocity.y < 0)
			{
				auxVel.y *= -1;
			}

			return auxVel;
		}

		void Ball::changeAngle()
		{
			if (position.x == (game::screenWidth / 2))
			{
				velocity.x = 0;
			}
			else
			{
				float porcX = 0;

				if (position.x < (game::screenWidth / 2))
				{
					porcX = 100 - ((game::screenWidth / 2) - position.x) * 100 / (game::screenWidth / 2);
					setAngleForX(porcX);
					velocity = getVelocityForAngle(angle, ballSpeed);
					velocity.x *= -1;
					velocity.y *= -1;
				}
				else
				{
					porcX = (game::screenWidth - position.x) * 100 / (game::screenWidth / 2);
					setAngleForX(porcX);
					velocity = getVelocityForAngle(angle, ballSpeed);
					velocity.y *= -1;
				}
			}
		}

		void Ball::collisionLimit()
		{
			if ((position.x - radius) <= 0)
			{
				position.x = radius;
				velocity.x *= -1;
			}
			else if ((position.x + radius) >= game::screenWidth)
			{
				position.x = game::screenWidth - radius;
				velocity.x *= -1;
			}
			else if ((position.y - radius) <= 0)
			{
				position.y = radius;
				velocity.y *= -1;
			}
		}

		void Ball::collisionPlayer(Rectangle paddle)
		{
			Vector2 p = getPaddlePerimeterPosition(paddle);
			if (checkCollisionPaddle(p))
			{
				if (p.y == paddle.y)
				{
					if (p.x == paddle.x + (paddle.width / 2))
					{
						velocity.x = 0;
					}
					else
					{
						float porcX = 0;

						if (p.x < paddle.x + (paddle.width / 2))
						{
							porcX = 100 - (paddle.x + (paddle.width / 2) - position.x) * 100 / (paddle.width / 2);
							setAngleForX(porcX);
							velocity = getVelocityForAngle(angle, ballSpeed);
							velocity.x *= -1;
						}
						else
						{
							porcX = (paddle.x + paddle.width - position.x) * 100 / (paddle.width / 2);
							setAngleForX(porcX);
							velocity = getVelocityForAngle(angle, ballSpeed);
						}
					}

					position.y = paddle.y - radius - 1;
					velocity.y *= -1;

					return;
				}

				changeDirection(p, paddle, false);
			}
		}

		void Ball::collisionBlock(Rectangle paddle, bool multiCollision)
		{
			Vector2 p = getPaddlePerimeterPosition(paddle);
			if (checkCollisionPaddle(p))
			{
				changeDirection(p, paddle, multiCollision);
			}
		}

		void Ball::changeDirection(Vector2 p, Rectangle paddle, bool multiCollision)
		{
			//Collision Up-Left
			if (p.y == paddle.y && p.x == paddle.x)
			{
				position.y = paddle.y - radius - 1;

				if (velocity.y > 0)
				{
					velocity.y *= -1;

					if (!multiCollision && velocity.x > 0)
					{
						velocity.x *= -1;
					}
				}
				else
				{
					velocity.x *= -1;
				}

				return;
			}

			//Collision Up-Right
			if (p.y == paddle.y && p.x == paddle.x + paddle.width)
			{
				position.y = paddle.y - radius - 1;

				if (velocity.y > 0)
				{
					velocity.y *= -1;

					if (!multiCollision && velocity.x < 0)
					{
						velocity.x *= -1;
					}
				}
				else
				{
					velocity.x *= -1;
				}

				return;
			}

			//Collision Down-Left
			if (p.y == paddle.y + paddle.height && p.x == paddle.x)
			{
				position.y = paddle.y + paddle.height + radius + 1;

				if (velocity.y < 0)
				{
					velocity.y *= -1;

					if (!multiCollision && velocity.x > 0)
					{
						velocity.x *= -1;
					}
				}
				else
				{
					velocity.x *= -1;
				}

				return;
			}

			//Collision Down-Right
			if (p.y == paddle.y + paddle.height && p.x == paddle.x + paddle.width)
			{
				position.y = paddle.y + paddle.height + radius + 1;

				if (velocity.y < 0)
				{
					velocity.y *= -1;

					if (!multiCollision && velocity.x < 0)
					{
						velocity.x *= -1;
					}
				}
				else
				{
					velocity.x *= -1;
				}

				return;
			}

			//Collision Up
			if (p.y == paddle.y)
			{
				position.y = paddle.y - radius - 1;
				velocity.y *= -1;

				return;
			}

			//Collision Down
			if (p.y == paddle.y + paddle.height)
			{
				position.y = paddle.y + paddle.height + radius + 1;
				velocity.y *= -1;

				return;
			}

			//Collision Left
			if (p.x == paddle.x)
			{
				position.x = paddle.x - radius - 1;
				velocity.x *= -1;

				return;
			}

			//Collision Right
			if (p.x == paddle.x + paddle.width)
			{
				position.x = paddle.x + paddle.width + radius + 1;
				velocity.x *= -1;

				return;
			}
		}

		bool Ball::checkCollisionPaddle(Rectangle paddle)
		{
			Vector2 p = getPaddlePerimeterPosition(paddle);

			double distance = sqrt((static_cast<double>(position.x) - static_cast<double>(p.x)) * (static_cast<double>(position.x) - static_cast<double>(p.x)) + (static_cast<double>(position.y) - static_cast<double>(p.y)) * (static_cast<double>(position.y) - static_cast<double>(p.y)));
			if (distance < radius)
			{
				return true;
			}

			return false;
		}

		bool Ball::checkCollisionPaddle(Vector2 p)
		{
			double distance = sqrt((static_cast<double>(position.x) - static_cast<double>(p.x)) * (static_cast<double>(position.x) - static_cast<double>(p.x)) + (static_cast<double>(position.y) - static_cast<double>(p.y)) * (static_cast<double>(position.y) - static_cast<double>(p.y)));
			if (distance < radius)
			{
				return true;
			}

			return false;
		}

		void Ball::show()
		{
			DrawTextureRec(texture, { 0, 0, (radius * 2), (radius * 2) }, { (position.x - radius), (position.y - radius )}, color);

			#if DEBUG
				DrawCircleLines(static_cast<int>(position.x), static_cast<int>(position.y), radius, GREEN);
			#endif // DEBUG
		}

		Vector2 Ball::getPosition()
		{
			return this->position;
		}

		void Ball::setPosition(Vector2 position)
		{
			this->position = position;
		}

		float Ball::getRadius()
		{
			return this->radius;
		}

		void Ball::setRadius(float radius)
		{
			this->radius = radius;
		}

		Color Ball::getColor()
		{
			return this->color;
		}

		void Ball::setColor(Color color)
		{
			this->color = color;
		}

		Texture2D Ball::getTexture()
		{
			return this->texture;
		}

		void Ball::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Vector2 Ball::getVelocity()
		{
			return this->velocity;
		}

		void Ball::setVelocity(Vector2 velocity)
		{
			this->velocity = velocity;
		}

		float Ball::getAngle()
		{
			return this->angle;
		}

		void Ball::setAngle(float angle)
		{
			this->angle = angle;
		}

		bool Ball::getCollision()
		{
			return this->collision;
		}

		void Ball::setCollision(bool collision)
		{
			this->collision = collision;
		}

		bool Ball::getFireball()
		{
			return this->fireball;
		}

		void Ball::setFireball(bool fireball)
		{
			this->fireball = fireball;
		}

		Vector2 Ball::getPaddlePerimeterPosition(Rectangle paddle)
		{
			Vector2 p = Vector2();

			p.x = position.x;
			if (p.x < paddle.x)
			{
				p.x = paddle.x;
			}
			else if (p.x > paddle.x + paddle.width)
			{
				p.x = paddle.x + paddle.width;
			}

			p.y = position.y;
			if (p.y < paddle.y)
			{
				p.y = paddle.y;
			}
			else if (p.y > paddle.y + paddle.height)
			{
				p.y = paddle.y + paddle.height;
			}

			return p;
		}

		void Ball::setAngleForX(float porcX)
		{
			angle = ballMinAngle + ((ballStartAngle - ballMinAngle) * porcX / 100);
		}
	}
}