#pragma once
#ifndef BALL_H
#define	BALL_H

#include <math.h>

#include "raylib.h"

namespace breakout
{
	namespace ball
	{
		extern float ballSpeed;
		extern float ballRadius;

		extern Vector2 ballStartPosition;
		extern Vector2 ballStartVelocity;

		void init();
		void start(int index, Vector2 pos, Vector2 vel, float angle);
		void reset();
		void deInit();
		Vector2 getVelocityForAngle(float angle, float velocityMax);

		class Ball
		{
		public:
			Ball();
			Ball(Vector2 position, float radius, Color color, Texture2D texture, Vector2 velocity, float angle, bool collision, bool fireball);
			~Ball();

			void move();
			void reset();
			Vector2 getNewVelocityForAngle(float angle);
			void changeAngle();
			void collisionLimit();
			void collisionPlayer(Rectangle paddle);
			void collisionBlock(Rectangle paddle, bool multiCollision);
			void changeDirection(Vector2 p, Rectangle paddle, bool multiCollision);
			bool checkCollisionPaddle(Rectangle paddle);
			bool checkCollisionPaddle(Vector2 p);
			void show();

			Vector2 getPosition();
			void setPosition(Vector2 position);
			float getRadius();
			void setRadius(float radius);
			Color getColor();
			void setColor(Color color);
			Texture2D getTexture();
			void setTexture(Texture2D texture);
			Vector2 getVelocity();
			void setVelocity(Vector2 velocity);
			float getAngle();
			void setAngle(float angle);
			bool getCollision();
			void setCollision(bool collision);
			bool getFireball();
			void setFireball(bool fireball);
		private:
			Vector2 position;
			float radius;
			Color color;
			Texture2D texture;
			Vector2 velocity;
			float angle;
			bool collision;
			bool fireball;

			Vector2 getPaddlePerimeterPosition(Rectangle paddle);
			void setAngleForX(float porcX);
		};
	}
}

#endif // !BALL_H