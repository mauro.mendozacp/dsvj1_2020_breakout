#pragma once
#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace breakout
{
	namespace input
	{
		extern int moveRightPlayer;
		extern int moveLeftPlayer;
		extern int shootBall;
		extern int pauseGame;

		bool checkMoveUpOption();
		bool checkMoveDownOption();
		bool checkMoveLeftOption();
		bool checkMoveRightOption();
		bool checkAcceptOption();
		bool checkMoveRightPlayer();
		bool checkMoveLeftPlayer();
		bool checkShootBall();
		bool checkPauseGame();
		bool checkMoveUpDownOption();
		bool checkMoveLeftRightOption();
		bool checkMoveRightLeftPlayer();

		int GetInputChange();
		const char* GetInputText(int iKey);
	}
}

#endif // !INPUT_H
