#include "texture.h"
#include "game/game.h"
#include "entities/player/player.h"
#include "entities/ball/ball.h"
#include "entities/block/block.h"

namespace breakout
{
	namespace texture
	{
		Texture2D paddle;
		Texture2D ball;
		Texture2D ballRed;
		Texture2D blueBlock;
		Texture2D yellowBlock;
		Texture2D redBlock;
		Texture2D greenBlock;
		Texture2D skyblueBlock;
		Texture2D unbreakableBlock;
		Texture2D bomb;
		Texture2D explosion;
		Texture2D gameplayBackground;
		Texture2D menuBackground;

		void setSizeTexture()
		{
			paddle.height = static_cast<int>(player::playerPaddleHeight);
			paddle.width = static_cast<int>(player::playerPaddleWidth);

			int diameter = static_cast<int>(ball::ballRadius * 2);
			ball.height = diameter;
			ball.width = diameter;
			ballRed.height = diameter;
			ballRed.width = diameter;

			int blockHeight = static_cast<int>(block::blockPaddleHeight);
			int blockWidth = static_cast<int>(block::blockPaddleWidth);

			blueBlock.height = blockHeight;
			blueBlock.width = blockWidth;

			yellowBlock.height = blockHeight;
			yellowBlock.width = blockWidth;

			redBlock.height = blockHeight;
			redBlock.width = blockWidth;

			greenBlock.height = blockHeight;
			greenBlock.width = blockWidth;

			skyblueBlock.height = blockHeight;
			skyblueBlock.width = blockWidth;

			unbreakableBlock.height = blockHeight;
			unbreakableBlock.width = blockWidth;

			bomb.height = blockHeight / 2;
			bomb.width = blockHeight / 2;

			explosion.height = blockHeight * 4;
			explosion.width = blockWidth * 3 * 8;

			gameplayBackground.height = game::screenHeight;
			gameplayBackground.width = game::screenWidth;

			menuBackground.height = game::screenHeight;
			menuBackground.width = game::screenWidth;
		}

		void init()
		{
			paddle = LoadTexture("res/assets/textures/paddle.png");
			ball = LoadTexture("res/assets/textures/ball.png");
			ballRed = LoadTexture("res/assets/textures/ball_red.png");
			blueBlock = LoadTexture("res/assets/textures/blue_block.png");
			yellowBlock = LoadTexture("res/assets/textures/yellow_block.png");
			redBlock = LoadTexture("res/assets/textures/red_block.png");
			greenBlock = LoadTexture("res/assets/textures/green_block.png");
			skyblueBlock = LoadTexture("res/assets/textures/skyblue_block.png");
			unbreakableBlock = LoadTexture("res/assets/textures/unbreakable_block.png");
			bomb = LoadTexture("res/assets/textures/bomb.png");
			explosion = LoadTexture("res/assets/textures/bomb_explosion_animation.png");
			gameplayBackground = LoadTexture("res/assets/textures/gameplay_background.png");
			menuBackground = LoadTexture("res/assets/textures/menu_background.png");

			setSizeTexture();
		}

		void deInit()
		{
			UnloadTexture(paddle);
			UnloadTexture(ball);
			UnloadTexture(ballRed);
			UnloadTexture(blueBlock);
			UnloadTexture(yellowBlock);
			UnloadTexture(redBlock);
			UnloadTexture(greenBlock);
			UnloadTexture(skyblueBlock);
			UnloadTexture(unbreakableBlock);
			UnloadTexture(bomb);
			UnloadTexture(explosion);
			UnloadTexture(gameplayBackground);
			UnloadTexture(menuBackground);
		}
	}
}