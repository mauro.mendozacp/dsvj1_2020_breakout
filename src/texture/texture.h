#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H

#include "raylib.h"

namespace breakout
{
	namespace texture
	{
		extern Texture2D paddle;
		extern Texture2D ball;
		extern Texture2D ballRed;
		extern Texture2D blueBlock;
		extern Texture2D yellowBlock;
		extern Texture2D redBlock;
		extern Texture2D greenBlock;
		extern Texture2D skyblueBlock;
		extern Texture2D unbreakableBlock;
		extern Texture2D bomb;
		extern Texture2D explosion;
		extern Texture2D gameplayBackground;
		extern Texture2D menuBackground;

		void init();
		void deInit();
		void setSizeTexture();
	}
}

#endif // !TEXTURE_H