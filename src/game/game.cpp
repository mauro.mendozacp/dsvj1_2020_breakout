#include "game.h"

namespace breakout
{
	namespace game
	{
		GAME_STATUS gameStatus;
		const char* titleGame = "BREAKOUT";

		int screenWidth = 840;
		int screenHeight = 650;
		const int frames = 60;
		int highScore = 0;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, titleGame);

			SetTargetFPS(frames);

			texture::init();
			audio::init();
			fonts::init();
			highScore = LoadStorageValue(STORAGE_DATA::STORAGE_POSITION_HISCORE);

			gameStatus = GAME_STATUS::MAIN_MENU;
			main_menu::init();
		}

		static void update()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::INGAME:
				gameplay::update();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::update();
				break;
			case GAME_STATUS::PAUSE:
				pause::update();
				break;
			case GAME_STATUS::WON_LEVEL:
				won_level::update();
				break;
			case GAME_STATUS::LOST_LEVEL:
				lost_level::update();
				break;
			case GAME_STATUS::FINISH_GAME:
				finish_game::update();
				break;
			case GAME_STATUS::OPTIONS:
				options::update();
				break;
			case GAME_STATUS::GAME_SETTINGS:
				game_settings::update();
				break;
			case GAME_STATUS::GAMEPLAY_SETTINGS:
				gameplay_settings::update();
				break;
			case GAME_STATUS::CONTROLS:
				controls::update();
				break;
			case GAME_STATUS::CREDITS:
				credits::update();
				break;
			default:
				break;
			}
		}

		static void drawBackground()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::INGAME:
			case GAME_STATUS::PAUSE:
			case GAME_STATUS::WON_LEVEL:
			case GAME_STATUS::LOST_LEVEL:
			case GAME_STATUS::FINISH_GAME:
				DrawTexture(texture::gameplayBackground, 0, 0, WHITE);
				break;
			case GAME_STATUS::MAIN_MENU:
			case GAME_STATUS::OPTIONS:
			case GAME_STATUS::GAME_SETTINGS:
			case GAME_STATUS::GAMEPLAY_SETTINGS:
			case GAME_STATUS::CONTROLS:
			case GAME_STATUS::CREDITS:
				DrawTexture(texture::menuBackground, 0, 0, WHITE);
				break;
			default:
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();

			ClearBackground(BLACK);

			drawBackground();

			switch (gameStatus)
			{
			case GAME_STATUS::INGAME:
				gameplay::draw();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::draw();
				break;
			case GAME_STATUS::PAUSE:
				pause::draw();
				break;
			case GAME_STATUS::WON_LEVEL:
				won_level::draw();
				break;
			case GAME_STATUS::LOST_LEVEL:
				lost_level::draw();
				break;
			case GAME_STATUS::FINISH_GAME:
				finish_game::draw();
				break;
			case GAME_STATUS::OPTIONS:
				options::draw();
				break;
			case GAME_STATUS::GAME_SETTINGS:
				game_settings::draw();
				break;
			case GAME_STATUS::GAMEPLAY_SETTINGS:
				gameplay_settings::draw();
				break;
			case GAME_STATUS::CONTROLS:
				controls::draw();
				break;
			case GAME_STATUS::CREDITS:
				credits::draw();
				break;
			default:
				break;
			}

			EndDrawing();
		}

		static void deInit()
		{
			if (gameStatus != GAME_STATUS::GAME_SETTINGS)
			{
				changeStatus(GAME_STATUS::EXIT);
			}

			audio::deInit();
			fonts::deInit();
			texture::deInit();
			CloseWindow();
		}

		void runGame()
		{
			init();
			while (!WindowShouldClose() && gameStatus != GAME_STATUS::EXIT)
			{
				update();
				draw();
			}
			deInit();
		}

		void changeStatus(GAME_STATUS newGameStatus)
		{
			switch (gameStatus)
			{
			case GAME_STATUS::INGAME:
				gameplay::deInit();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::deInit();
				break;
			case GAME_STATUS::PAUSE:
				pause::deInit();
				break;
			case GAME_STATUS::WON_LEVEL:
				won_level::deInit();
				break;
			case GAME_STATUS::LOST_LEVEL:
				lost_level::deInit();
				break;
			case GAME_STATUS::FINISH_GAME:
				finish_game::deInit();
				break;
			case GAME_STATUS::OPTIONS:
				options::deInit();
				break;
			case GAME_STATUS::GAME_SETTINGS:
				game_settings::deInit();
				break;
			case GAME_STATUS::GAMEPLAY_SETTINGS:
				gameplay_settings::deInit();
				break;
			case GAME_STATUS::CONTROLS:
				controls::deInit();
				break;
			case GAME_STATUS::CREDITS:
				credits::deInit();
				break;
			case GAME_STATUS::EXIT:
				break;
			default:
				break;
			}

			switch (newGameStatus)
			{
			case GAME_STATUS::INGAME:
				gameplay::init();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::init();
				break;
			case GAME_STATUS::PAUSE:
				pause::init();
				break;
			case GAME_STATUS::WON_LEVEL:
				won_level::init();
				break;
			case GAME_STATUS::LOST_LEVEL:
				lost_level::init();
				break;
			case GAME_STATUS::FINISH_GAME:
				finish_game::init();
				break;
			case GAME_STATUS::OPTIONS:
				options::init();
				break;
			case GAME_STATUS::GAME_SETTINGS:
				game_settings::init();
				break;
			case GAME_STATUS::GAMEPLAY_SETTINGS:
				gameplay_settings::init();
				break;
			case GAME_STATUS::CONTROLS:
				controls::init();
				break;
			case GAME_STATUS::CREDITS:
				credits::init();
				break;
			case GAME_STATUS::EXIT:
				break;
			default:
				break;
			}

			gameStatus = newGameStatus;
		}
	}
}