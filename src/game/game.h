#pragma once
#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#include "font/font.h"
#include "input/input.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/main_menu/main_menu.h"
#include "scenes/pause/pause.h"
#include "scenes/won_level/won_level.h"
#include "scenes/lost_level/lost_level.h"
#include "scenes/finish_game/finish_game.h"
#include "scenes/options/options.h"
#include "scenes/game_settings/game_settings.h"
#include "scenes/gameplay_settings/gameplay_settings.h"
#include "scenes/controls/controls.h"
#include "scenes/credits/credits.h"

namespace breakout
{
	namespace game
	{
		enum STORAGE_DATA
		{
			STORAGE_POSITION_HISCORE = 0
		};

		enum class GAME_STATUS
		{
			INGAME = 1,
			MAIN_MENU,
			PAUSE,
			WON_LEVEL,
			LOST_LEVEL,
			FINISH_GAME,
			OPTIONS,
			GAME_SETTINGS,
			GAMEPLAY_SETTINGS,
			CONTROLS,
			CREDITS,
			EXIT
		};

		extern GAME_STATUS gameStatus;
		extern const char* titleGame;

		extern int screenWidth;
		extern int screenHeight;
		extern const int frames;
		extern int highScore;

		void runGame();
		void changeStatus(GAME_STATUS newGameStatus);
	}
}

#endif // !GAME_H